<?php namespace inmotek\idealista\v6\model;

class NewDevelopment extends CoreProperty implements verificaciones {
    /**
     * @var array \inmotek\idealista\v6\model\Typology
     */
    public $newDevelopmentTypologies;
  
    /**
     * @var \inmotek\idealista\v6\model\Promo
     */
    public \inmotek\idealista\v6\model\Promo $propertyFeatures;
    
    /**
     * array de tipo typology
     * @param array $newDevelopmentTypologies de inmotek\idealista\v6\model\Typology
     * @return $this
     */
    public function setNewDevelopmentTypologies($newDevelopmentTypologies) : self{
        $this->newDevelopmentTypologies = $newDevelopmentTypologies;
        return $this;
    }
    
    public function setPropertyFeatures(\inmotek\idealista\v6\model\Promo $propertyFeatures) : self{
        $this->propertyFeatures = $propertyFeatures;
        return $this;
    }
    
    
    public function verificaciones() {
        parent::verificaciones();
    }
}
