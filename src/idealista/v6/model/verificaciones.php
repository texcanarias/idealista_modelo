<?php namespace inmotek\idealista\v6\model;

/**
 *
 * @author antonio
 */
interface verificaciones {
    
    /**
     * Verifica si la clase cumple con las condiciones para ser procesado por el ws de idealista
     * @return array  El array tiene dos partes verificacion que es booleano (true es que se cumple las condiciones , false fallo) y errores un array con texto descriptivo.
     */
    public function verificaciones();
}
