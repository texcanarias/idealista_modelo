<?php namespace inmotek\idealista\v6\model;

class Promo implements \inmotek\idealista\v6\model\verificaciones {
    use \inmotek\idealista\v6\model\traitLimpieza;
    use \inmotek\idealista\v6\model\traitErrores;
    use \inmotek\idealista\v6\model\feature\traits\featureEnergyCertificate;
    use \inmotek\idealista\v6\model\feature\traits\featureConservation;
    use \inmotek\idealista\v6\model\feature\traits\featureAccessType;
    use \inmotek\idealista\v6\model\feature\traits\featureSecurity;

    static string $TYPE_PROMO_RESTORED_BUILDING = "restored_building";
    static string $TYPE_PROMO_NEW_BUILDING = "new_building";
    static string $TYPE_PROMO_HOUSE = "house";
    static string $TYPE_PROMO_MIXED_PROMOS = "mixed_promos";
    
    static string $AVAILABILITY_HOUR_ANY_MORNING = "any_morning";
    static string $AVAILABILITY_HOUR_MORNINGS = "mornings";
    static string $AVAILABILITY_HOUR_AT_NOON = "at_noon";
    static string $AVAILABILITY_HOUR_AFTERNOONS = "afternoons";
    static string $AVAILABILITY_HOUR_NIGHTS = "nights";
    static string $AVAILABILITY_HOUR_WEEKENDS = "weekends";
    static string $AVAILABILITY_HOUR_BUSINESS_HOUR = "business_hour";

    public string $featuresType;
    public bool $featuresLiftAvailable;
    public bool $featuresDoorman;
    public bool $featuresPool;
    public bool $featuresGarden;    
    
    public string $featuresNewDevelopmentType;
    public bool $featuresNewDevelopmentName;
    public bool $featuresFinished;
    public string $featuresStartDate;
    public int $featuresKeyDeliveryYear;
    public int $featuresKeyDeliveryMonth;
    public int $featuresBuiltPhase;
    public string $featuresAvailabilityHour;
    /**
     * @var array \inmotek\idealista\v6\model\Description
     */
    public $featuresAccessComments;
    public bool $featuresOnSite;
    
    public int $featuresWindowsNumber;

    /**
     * Las siguientes propiedades están sin implementar 
     * a la espera de una próxima versión donde esté soportada
     * por inmotek
     */
    public $featuresMortgageStateSubsidized;
    public $featuresMortgageBankName;
    public $featuresMortgagePercentage;
    public $featuresMortgageEntryPercentage;
    public $featuresMortgageLettersPercentage;
    public $featuresMortgageInterestRate;
    public $featuresMortgageYears;
    
    
    public function __construct() {
        $this->featuresType = "promo";
        $this->featuresNewDevelopmentType = self::$TYPE_PROMO_NEW_BUILDING;
    }
    
    /**
     * only for flat type
     * @param boolean $featuresLiftAvailable
     * @return $this
     */
    public function setFeaturesLiftAvailable(bool $featuresLiftAvailable) : self{
        $this->featuresLiftAvailable = $featuresLiftAvailable;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresDoorman
     * @return $this
     */
    public function setFeaturesDoorman(bool $featuresDoorman) : self{
        $this->featuresDoorman = $featuresDoorman;
        return $this;
    }
    
    /**
     * pool availability
     * @param boolean $featuresPool
     * @return $this
     */
    public function setFeaturesPool(bool $featuresPool) : self{
        $this->featuresPool = $featuresPool;
        return $this;
    }
    
    /**
     * 
     * @param boolean $featuresGarden
     * @return $this
     */
    public function setFeaturesGarden(bool $featuresGarden) : self{
        $this->featuresGarden = $featuresGarden;
        return $this;
    }
    
    
    public function setFeaturesNewDevelopmentName(string $featuresNewDevelopmentName) : self{
        if("" != $featuresNewDevelopmentName){
            $this->addressStreetName = mb_substr($featuresNewDevelopmentName, 0, 100);
        }
        return $this;
    }
    
    public function setFeaturesFinished(bool $featuresFinished) : self{
        $this->featuresFinished = $featuresFinished;
        return $this;
    }

    /**
     * Fecha con formato "^20[0-9][0-9]/(0[1-9]|1[0-2])/(([0-2][0-9])|(3[0-1]))$"
     * @param type $featuresStartDate
     * @return $this
     */
    public function setfeaturesStartDate(string $featuresStartDate) : self{
        if("0000-00-00" != $featuresStartDate){
            $featuresStartDate = str_replace( "-" , "/" , $featuresStartDate);
            $this->featuresStartDate = $featuresStartDate;
        }
        return $this;
    }
        
    public function setFeaturesKeyDeliveryYear(int $featuresKeyDeliveryYear) : self{
        if ("" != $featuresKeyDeliveryYear && 0 != $featuresKeyDeliveryYear) {
            if (1 <= $featuresKeyDeliveryYear && 9999 >= $featuresKeyDeliveryYear) {
                $this->featuresKeyDeliveryYear = (int)$featuresKeyDeliveryYear;
            } else {
                $this->setErrores("featuresKeyDeliveryYear debe estar entre 1 y 9999 y es $featuresKeyDeliveryYear");
            }
        }
        return $this;
    }

    public function setFeaturesKeyDeliveryMonth(int $featuresKeyDeliveryMonth) :self{
        if ("" != $featuresKeyDeliveryMonth && 0 != $featuresKeyDeliveryMonth) {
            if (1 <= $featuresKeyDeliveryMonth && 12 >= $featuresKeyDeliveryMonth) {
                $this->featuresKeyDeliveryMonth = (int)$featuresKeyDeliveryMonth;
            } else {
                $this->setErrores("featuresKeyDeliveryMonth debe estar entre 1 y 12 y es $featuresKeyDeliveryMonth");
            }
        }
        return $this;
    }

    public function setFeaturesBuiltPhase(int $featuresBuiltPhase) : self{
        $this->featuresBuiltPhase = $featuresBuiltPhase;
        return $this;
    }

    public function setFeaturesAvailabilityHour(string $featuresAvailabilityHour) : self{
        $this->featuresAvailabilityHour = $featuresAvailabilityHour;
        return $this;
    }
 
    /**
     * 
     * @param array $featuresAccessComments de \inmotek\idealista\v6\model\Description
     */
    public function setFeaturesAccessComments($featuresAccessComments){
        $this->featuresAccessComments = $featuresAccessComments;
    }


    /**
     * @param bool $featuresOnSite Booth sale on side
     * @return $this
     */
    public function setFeaturesOnSite(bool $featuresOnSite) : self{
        $this->featuresOnSite = $featuresOnSite;
        return $this;
    }

            
    public function setFeaturesWindowsNumber(int $featuresWindowsNumber) : self{
        if ("" != $featuresWindowsNumber && 0 != $featuresWindowsNumber) {
            if (1 <= $featuresWindowsNumber && 999 >= $featuresWindowsNumber) {
                $this->featuresWindowsNumber = (int)$featuresWindowsNumber;
            } else {
                $this->setErrores("featuresWindowsNumber debe estar entre 1 y 999 y es $featuresWindowsNumber");
            }
        }
        return $this;
    }
    
    
    public function verificaciones() {
        $verificacion01 = "" != $this->featuresType && "" != $this->featuresNewDevelopmentName;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene featuresType y/o featuresNewDevelopmentName  {featuresType:$this->featuresType, featuresNewDevelopmentName:$this->featuresNewDevelopmentName}";
        }
        
        return ["verificacion" => $verificacion01, "errores" => $this->errores];        
    }
 
    

    /**
     * Set the value of featuresNewDevelopmentType
     *
     * @return  self
     */ 
    public function setFeaturesNewDevelopmentType(string $featuresNewDevelopmentType) : self
    {
        $this->featuresNewDevelopmentType = $featuresNewDevelopmentType;

        return $this;
    }
}