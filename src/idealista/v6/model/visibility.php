<?php namespace inmotek\idealista\v6\model;

trait visibility {
    static string $VISIBILITY_PRIVATE = "private";
    static string $VISIBILITY_IDEALISTA = "idealista";
    static string $VISIBILITY_MICROSITE = "microsite";

    public string $propertyVisibility = "private";

    /**
     * If the visibility is 'idealista', you can find the property using the
     * idealista's search engine; 'microsite', the property is only published
     * on the real estate agency microsite; 'private', the property is not
     * published and only the customer can see it.
     * @param type $propertyVisibility
     * @return $this
     */
    public function setPropertyVisibility(string $propertyVisibility) : self{
        $this->propertyVisibility = $propertyVisibility;
        if ("" == $this->propertyVisibility){
          $this->propertyVisibility = self::$VISIBILITY_PRIVATE;
        }
        return $this;
    }

}
