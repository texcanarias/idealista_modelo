<?php namespace inmotek\idealista\v6\model;

/**
 * Documentación oficial de idealista https://feeds.idealista.com/v6/schemas/properties/typology.json
 */
class Typology extends CorePropertyTypology implements verificaciones {  
    
    public \inmotek\idealista\v6\model\Operation $propertyOperation;

    /**
     * 
     * @param type $propertyFeatures
     */
    function setPropertyFeatures(\inmotek\idealista\v6\model\feature\Feature $propertyFeatures) : self{
        if (    is_a($propertyFeatures, 'garage') || 
                is_a($propertyFeatures, 'premises') || 
                is_a($propertyFeatures, 'offices') || 
                is_a($propertyFeatures, 'homes')){
            $this->propertyFeatures = $propertyFeatures;
        }
        else{
            throw new \Exception("propertyFeatures no es una instancia de garage, premises, offices o homes");
        }
        return $this;
    }

    /**
     * 
     * @param \inmotek\idealista\v6\model\ $propertyOperation
     */
    function setPropertyOperation(\inmotek\idealista\v6\model\Operation $propertyOperation) : self{
        $this->propertyOperation = $propertyOperation;
        return $this;
    }

        
    public function verificaciones() {
        $verificacion01 = "" != $this->propertyCode;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene propertyCode {propertyCode:$this->propertyCode}";
        }
        
        return ["verificacion" => $verificacion01 , "errores" => $this->errores];         
    }
}