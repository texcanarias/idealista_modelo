<?php namespace inmotek\idealista\v6\model;

class CorePropertyTypology extends CoreProperty implements verificaciones {
  
    /**
     * @var \inmotek\idealista\v6\model\Feature
     */
    public \inmotek\idealista\v6\model\feature\Feature $propertyFeatures;
    
    
    public function setPropertyFeatures(\inmotek\idealista\v6\model\feature\Feature $propertyFeatures) : self{
        $this->propertyFeatures = $propertyFeatures;
        return $this;
    }
    
    
    public function verificaciones() {
        parent::verificaciones();
    }
}
