<?php namespace inmotek\idealista\v6\model\feedback;

class Property{
    private $date;
    private $hour;
    private $propertyId;
    private $propertyCode;
    private $propertyReference;
    private $propertyStatus;
    private $propertyVisibility;
    private $propertyOperation;
    private $propertyLink;
    private $inmueble_id;
    
    public function getDate() {
        return $this->date;
    }

    public function getHour() {
        return $this->hour;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    public function setHour($hour) {
        $this->hour = $hour;
        return $this;
    }
    
    public function getPropertyId() {
        return $this->propertyId;
    }

    public function getPropertyCode() {
        return $this->propertyCode;
    }

    public function getPropertyReference() {
        return $this->propertyReference;
    }

    public function getPropertyStatus() {
        return $this->propertyStatus;
    }

    public function getPropertyVisibility() {
        return $this->propertyVisibility;
    }

    public function getPropertyOperation() {
        return $this->propertyOperation;
    }

    public function getPropertyLink() {
        return $this->propertyLink;
    }
    
    public function getInmueble_id() {
        return $this->inmueble_id;
    }

    public function setPropertyId($propertyId) {
        $this->propertyId = $propertyId;
        return $this;
    }

    public function setPropertyCode($propertyCode) {
        $partes = [];
        $partes = explode("_", $propertyCode);
        $this->propertyCode = $propertyCode;
        $this->inmueble_id = $partes[1];
        return $this;
    }

    public function setPropertyReference($propertyReference) {
        $this->propertyReference = $propertyReference;
        return $this;
    }

    public function setPropertyStatus($propertyStatus) {
        $this->propertyStatus = $propertyStatus;
        return $this;
    }

    public function setPropertyVisibility($propertyVisibility) {
        $this->propertyVisibility = $propertyVisibility;
        return $this;
    }

    public function setPropertyOperation($propertyOperation) {
        $this->propertyOperation = $propertyOperation;
        return $this;
    }

    public function setPropertyLink($propertyLink) {
        $this->propertyLink = $propertyLink;
        return $this;
    }



}