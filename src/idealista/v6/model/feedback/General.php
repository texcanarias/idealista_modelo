<?php namespace inmotek\idealista\v6\model\feedback;

class General  {
    private $customerCode;
    private $customerReference;
    private $properties;
    
    public function __construct() {
        $this->properties = [];
    }
    
    public function getCustomerCode() {
        return $this->customerCode;
    }

    public function getCustomerReference() {
        return $this->customerReference;
    }

    public function setCustomerCode($customerCode) {
        $this->customerCode = $customerCode;
        return $this;
    }

    public function setCustomerReference($customerReference) {
        $this->customerReference = $customerReference;
        return $this;
    }

    public function addPropertyToProperties(Property $property){
        $this->properties[] = $property;
    }
    
    public function getProperties(){
        return $this->properties;
    }

}