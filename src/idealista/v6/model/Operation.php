<?php namespace inmotek\idealista\v6\model;

class Operation implements verificaciones {

    use \inmotek\idealista\v6\model\traitLimpieza;
    use \inmotek\idealista\v6\model\traitErrores;

    public static string $RENT = "rent";
    public static string $SALE = "sale";
    public static string $RENT_TO_OWN = "rentToOwn";


    public ?int $operationDepositMonths = null;
    public ?int $operationPrice = null;
    public ?int $operationPriceCommunity = null;
    public ?int $operationPriceToOwn = null;
    public ?int $operationPriceTransfer = null;
    public ?int $operationPriceParking = null;
    public string $operationType = "rent";

    /**
     * deposit months
     * @param integer $operationDepositMonths
     * @return $this
     * @throws \Exception
     */
    public function setOperationDepositMonths(int $operationDepositMonths) : self{
        if ("" != $operationDepositMonths) {
            if (0 <= $operationDepositMonths && 12 >= $operationDepositMonths) {
                $this->operationDepositMonths = (int)$operationDepositMonths;
            } else {
                $this->setErrores("operationDepositMonths debe estar entre 0 y 12 y es $operationDepositMonths");
            }
        }
        return $this;
    }

    /**
     * operation price
     * @param integer $operationPrice
     * @return $this
     * @throws \Exception
     */
    public function setOperationPrice(int $operationPrice) : self{
        if ("" != $operationPrice) {
            if (1 <= $operationPrice && 99999999 >= $operationPrice) {
                $this->operationPrice = (int)$operationPrice;
            } else {
                $this->setErrores("operationPrice debe estar entre 1 y 99999999 y es $operationPrice");
            }
        }
        return $this;
    }

    /**
     * community fees - for Spain and Portugal, this field only applies to sale properties
     * @param type $operationPriceCommunity
     * @return $this
     */
    public function setOperationPriceCommunity(int $operationPriceCommunity) : self{
        if ("" != $operationPriceCommunity) {
            if (1 <= $operationPriceCommunity && 9999 >= $operationPriceCommunity) {
                $this->operationPriceCommunity = (int)$operationPriceCommunity;
            } else {
                $this->setErrores("operationPriceCommunity debe estar entre 1 y 9999 y es $operationPriceCommunity");
            }
        }
        return $this;
    }

    /**
     * operation price to own - this operation only applies to new development properties. Not avaible for second hand properties
     * @param integer $operationPriceToOwn
     * @return $this
     */
    public function setOperationPriceToOwn(int $operationPriceToOwn) : self{
        if ("" != $operationPriceToOwn) {
            if (1 <= $operationPriceToOwn && 99999999 >= $operationPriceToOwn) {
                $this->operationPriceToOwn = (int)$operationPriceToOwn;
            } else {
                $this->setErrores("operationPriceToOwn debe estar entre 1 y 99999999 y es $operationPriceToOwn");
            }
        }
        return $this;
    }

    /**
     * transfer price - this operation is only for premises typologies with rent operation
     * @param type $operationPriceTransfer
     * @return $this
     */
    public function setOperationPriceTransfer(int $operationPriceTransfer) : self{
        if ("" != $operationPriceTransfer) {
            if (1 <= $operationPriceTransfer && 99999999 >= $operationPriceTransfer) {
                $this->operationPriceTransfer = (int)$operationPriceTransfer;
            } else {
                $this->setErrores("operationPriceTransfer debe estar entre 1 y 99999999 y es $operationPriceTransfer");
            }
        }
        return $this;
    }

    /**
     * parking price
     * @param type $operationPriceParking
     * @return $this
     */
    public function setOperationPriceParking(int $operationPriceParking) : self{
        if ("" != $operationPriceParking) {
            if (1 <= $operationPriceParking && 99999999 >= $operationPriceParking) {
                $this->operationPriceParking = (int)$operationPriceParking;
            } else {
                $this->setErrores("operationPriceParking debe estar entre 1 y 99999999 y es $operationPriceParking");
            }
        }
        return $this;
    }

    /**
     * ["rent","sale","rentToOwn"]
     * @param string $operationType
     * @return $this
     */
    public function setOperationType(string $operationType) : self{
        //$this->operationType = self::$conversionOperationType[$operationType];
        $this->operationType = $operationType;
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->operationType && "" != $this->operationPrice;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene operationType y/o operationPrice {operationType:$this->operationType, operationPrice:$this->operationPrice}";
        }
        
        return ["verificacion" => $verificacion01 , "errores" => $this->errores];         
    }
    
}