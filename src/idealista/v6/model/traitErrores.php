<?php namespace inmotek\idealista\v6\model;

trait traitErrores {

    /**
     * Colección de los errores generados en el seteo 
     * @var array de strings
     */
    protected $errores = [];

    /**
     * Recuperar el listado de errores
     * @return array
     */
    function getErrores() {
        return $this->errores;
    }

    /**
     * Meter un error en la colección
     * @param string $errores
     */
    function setErrores($errores) {
        $this->errores[] = $errores;
    }

    
    function isError(){
        return (0 == count($this->errores))?false:true;
    }

}