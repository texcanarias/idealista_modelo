<?php namespace inmotek\idealista\v6\model;

class Address implements verificaciones {    
    use \inmotek\idealista\v6\model\traitLimpieza;
    use \inmotek\idealista\v6\model\traitErrores;

    static public string $VISIBILITY_FULL = "full";
    static public string $VISIBILITY_STREET = "street";
    static public string $VISIBILITY_HIDDEN = "hidden";

    static public string $COUNTRY_ES = "Spain";
    static public string $COUNTRY_IT = "Italy";
    static public string $COUNTRY_PT = "Portugal";
    static public string $COUNTRY_AN = "Andorra";
    static public string $COUNTRY_FR = "France";
    static public string $COUNTRY_SZ = "Switzerland";
    static public string $COUNTRY_SM = "San Marino";

    static public string $PRECISION_EXACT = "exact";
    static public string $PRECISION_MOVED = "moved";

    
    public string $addressVisibility = "";
    public string $addressStreetName = "";
    public string $addressStreetNumber = "";
    public string $addressBlock = "";
    public string $addressFloor = "";
    public string $addressStair = "";
    public string $addressDoor = "";
    public string $addressUrbanization = "";
    public string $addressPostalCode = "";
    public string $addressTown = "";
    public string $addressNsiCode = "";
    public string $addressCountry = "";
    public string $addressCoordinatesPrecision = "";
    public float $addressCoordinatesLatitude = 0;
    public float $addressCoordinatesLongitude = 0;

    public function setAddressVisibility(string $addressVisibility) : self {
        $this->addressVisibility = $addressVisibility;
        if("" == $this->addressVisibility){
          $this->addressVisibility = "hidden";
        }
        return $this;
    }

    public function setAddressStreetName(string $addressStreetName) : self{
        if("" != $addressStreetName){
            $this->addressStreetName = mb_substr($addressStreetName, 0, 200);
        }
        return $this;
    }

    public function setAddressStreetNumber(string $addressStreetNumber) : self{
        if("" != $addressStreetNumber){
            $this->addressStreetNumber = mb_substr($addressStreetNumber, 0, 10);
        }
        return $this;
    }

    public function setAddressBlock(string $addressBlock) : self{
        if("" != $addressBlock){
            $this->addressBlock = mb_substr($addressBlock, 0, 20);
        }
        return $this;
    }

    public function setAddressFloor(string $addressFloor) : self{
        $this->addressFloor = "";
        if ("" != $addressFloor) {
            $preg = "/(-[1-2]|[1-9]|[1-5][0-9]|60|bj|en|ss|st)/";
            if (preg_match($preg, $addressFloor)) {
                $this->addressFloor = $addressFloor;
            } else {
                $this->addressFloor = $addressFloor; 
                $this->setErrores("addressFloor no cumple con su pattern -> " . $addressFloor . " " . $preg);
            }
        }
        return $this;
    }

    public function setAddressStair(string $addressStair) : self{
        if("" != $addressStair){
            $this->addressStair = mb_substr($addressStair, 0, 10);
        }
        return $this;
    }

    public function setAddressDoor(string $addressDoor) : self{
        if ("" != $addressDoor){
            $this->addressDoor = mb_substr($addressDoor, 0, 4);
        }
        return $this;
    }

    public function setAddressUrbanization(string $addressUrbanization) : self{
        if("" != $addressUrbanization){
            $this->addressUrbanization = mb_substr($addressUrbanization, 0, 50);
        }
        return $this;
    }

    public function setAddressPostalCode(string $addressPostalCode) : self{
        $this->addressPostalCode = "";
        if ("" != $addressPostalCode) {
            $preg = "/[0-9]{5}$|^AD[0-9]{3}$|^[0-9]{4}(-[0-9]{3})/";
            if (preg_match($preg, $addressPostalCode)) {
                $this->addressPostalCode = $addressPostalCode;
            } else {
                $this->setErrores("addressPostalCode no cumple con su pattern -> " . $addressPostalCode . " " . $preg);
            }
        }

        return $this;
    }

    public function setAddressTown(string $addressTown) : self{
        if("" != $addressTown){
            $this->addressTown = mb_substr($addressTown, 0, 50);
        }
        return $this;
    }

    public function setAddressNsiCode(string $addressNsiCode) : self{
        $this->addressNsiCode = "";
        if ("" != $addressNsiCode) {
            $preg = "/[0-9]{6}/";
            if (preg_match($preg, $addressNsiCode)) {
                $this->addressNsiCode = $addressNsiCode;
            } else {
                $this->setErrores("addressNsiCode no cumple con su pattern -> " . $addressNsiCode . " " . $preg);
            }
        }
        return $this;
    }

    public function setAddressCountry(string $addressCountry) : self{
        //$this->addressCountry = self::$conversionPais[$addressCountry];
        $this->addressCountry = $addressCountry;
        if("" == $this->addressCountry){
          $this->addressCountry = "Spain";
        }
        return $this;
    }

    public function setAddressCoordinatesPrecision(string $addressCoordinatesPrecision) : self{
        $this->addressCoordinatesPrecision = $addressCoordinatesPrecision;
        return $this;
    }

    public function setAddressCoordinatesLatitude(float $addressCoordinatesLatitude) : self{
        $this->addressCoordinatesLatitude = 0;
        if ("" != $addressCoordinatesLatitude) {
            if (-90 <= $addressCoordinatesLatitude && 90 >= $addressCoordinatesLatitude) {
                $this->addressCoordinatesLatitude = $addressCoordinatesLatitude;
            } else {
                $this->setErrores("addressCoordinatesLatitude no cumple con su pattern");
            }
        }
        return $this;
    }

    public function setAddressCoordinatesLongitude(float $addressCoordinatesLongitude) : self{
      $this->addressCoordinatesLongitude = 0;
        if ("" != $addressCoordinatesLongitude) {
            if (-180 <= $addressCoordinatesLongitude && 180 >= $addressCoordinatesLongitude) {
                $this->addressCoordinatesLongitude = $addressCoordinatesLongitude;
            } else {
                $this->setErrores("addressCoordinatesLongitude no cumple con su pattern");
            }
        }
        return $this;
    }

    public function verificaciones() {
        $verifica01 = "" != $this->addressStreetName && "" != $this->addressCountry && "" != $this->addressPostalCode;
        $verifica02 = "" != $this->addressStreetName && "" != $this->addressCountry && "" != $this->addressTown;
        $verifica03 = "" != $this->addressCoordinatesLatitude && "" !=  $this->addressCoordinatesLongitude;
        $verifica04 = "" != $this->addressPostalCode && "" != $this->addressCountry;

        if(!$verifica01){
            $this->errores[] = "No tiene addressStreetName y/o addressPostalcode y/o addressCountry {addressStreetName:$this->addressStreetName, addressPostalcode:$this->addressPostalCode, addressCountry:$this->addressCountry}";
        }
        if(!$verifica02){
            $this->errores[] = "No tiene addressStreetName y/o addressTown y/o addressCountry {addressStreetName:$this->addressStreetName, addressTown:$this->addressTown, addressCountry:$this->addressCountry}";
        }
        if(!$verifica03){
            $this->errores[] = "No tiene addressCoordinatesLatitude y/o addressCoordinatesLongitude  {addressCoordinatesLatitude:$this->addressCoordinatesLatitude, addressCoordinatesLongitude:$this->addressCoordinatesLongitude, addressCoordinatesLongitude:$this->addressCoordinatesLongitude}";
        }
        if(!$verifica04){
            $this->errores[] = "No tiene addressPostalcode y/o addressCountry  {addressPostalcode:$this->addressPostalCode, addressCountry:$this->addressCountry}";
        }
        
        
        
        return ["verificacion" => ($verifica01 || $verifica02 || $verifica03 || $verifica04) , "errores" => $this->errores];           
    }

}
