<?php namespace inmotek\idealista\v6\model;

class Image implements verificaciones {
    
    use \inmotek\idealista\v6\model\traitLimpieza;
    use \inmotek\idealista\v6\model\traitErrores;

    static string $BASEMENT = "basement";
    static string $BATHROOM = "bathroom";
    static string $BEDROOM = "bedroom";
    static string $CELLAR = "cellar";
    static string $COMMUNAL_AREAS = "communalareas";
    static string $DINING_ROOM = "dining_room";
    static string $FACADE = "facade";
    static string $GARAGE = "garage";
    static string $HALL = "hall";
    static string $KITCHEN = "kitchen";
    static string $LIVING = "living";
    static string $OFFICE = "office";
    static string $PATIO = "patio";
    static string $PENTHOUSE = "penthouse";
    static string $PLAN = "plan";
    static string $POOL = "pool";
    static string $PORCH = "porch";
    static string $RECEPTION = "reception";
    static string $STORAGE = "storage";
    static string $SURROUNDINGS = "surroundings";
    static string $TERRACE = "terrace";
    static string $UNKNOWN = "unknown";
    static string $DETAILS = "details";
    static string $VIEWS = "views";

    public string $imageLabel = "";
    public int $imageOrder = 1;
    public string $imageUrl = "";

    public function setImageLabel(string $imageLabel) : self{
        $this->imageLabel = $imageLabel;
        return $this;
    }

    public function setImageOrder(int $imageOrder) : self{
        if (1 <= $imageOrder && 200 >= $imageOrder) {
            $this->imageOrder = (int)$imageOrder;
        } else {
            $this->setErrores("imageOrder debe estar entre 1 y 200 y es $imageOrder");
        }

        return $this;
    }

    public function setImageUrl(string $imageUrl) : self{
        $preg = "/(http)(s?):\/\/.*/";
        if (preg_match($preg, $imageUrl)) {
            $this->imageUrl = $imageUrl;
        } else {
            $this->setErrores("imageUrl no cumple con su pattern -> " . $imageUrl . " " . $preg);
        }
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->imageUrl;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene imageUrl {imageUrl:$this->imageUrl}";
        }
        
        return ["verificacion" => $verificacion01 , "errores" => $this->errores];         
        
    }

}
