<?php namespace inmotek\idealista\v6\model;

class CoreProperty implements verificaciones {

    use visibility;
    
    use \inmotek\idealista\v6\model\traitLimpieza;
    use \inmotek\idealista\v6\model\traitErrores;

    public string $propertyCode = "";
    public string $propertyReference = "";
    public string $propertyUrl = "";

    /**
     * @var \inmotek\idealista\v6\model\Address
     */
    public \inmotek\idealista\v6\model\Address $propertyAddress;

    /**
     * @var \inmotek\idealista\v6\model\Contact
     */
    public \inmotek\idealista\v6\model\Contact $propertyContact;

    /**
     * @var array \inmotek\idealista\v6\model\Description
     */
    public $propertyDescriptions;

    /**
     * @var array \inmotek\idealista\v6\model\Images
     */
    public $propertyImages;

    public function __construct() {
        $this->propertyVisibility = self::$VISIBILITY_PRIVATE;
    }

    public function setPropertyCode(string $propertyCode) : self{
        $this->propertyCode = mb_substr($propertyCode, 0, 50);
        return $this;
    }

    public function setPropertyReference(string $propertyReference) : self{
        $this->propertyReference = mb_substr($propertyReference, 0, 50);
        return $this;
    }

    public function setPropertyUrl(string $propertyUrl) : self{
        $this->propertyUrl = "";
          if ("" != $propertyUrl) {
              $preg = "/(http)(s?):\/\/.*/";
              if (preg_match($preg, $propertyUrl)) {
                  $this->propertyUrl = $propertyUrl;
              } else {
                  $this->setErrores("propertyUrl no cumple con su pattern -> " . $propertyUrl . " " . $preg);
              }
          }
          return $this;
      } 


    public function setPropertyContact(\inmotek\idealista\v6\model\Contact $propertyContact) : self{
        $this->propertyContact = $propertyContact;
        return $this;
    }

    public function setPropertyAddress(\inmotek\idealista\v6\model\Address $propertyAddress) : self{
        $this->propertyAddress = $propertyAddress;
        return $this;
    }

    /**
     * @param array propertyDescriptions
     */
    public function setPropertyDescriptions($propertyDescriptions) : self{
        $this->propertyDescriptions = $propertyDescriptions;
        return $this;
    }

    /**
     * @param array propertyImages
     */
    public function setPropertyImages($propertyImages) : self{
        $this->propertyImages = $propertyImages;
        return $this;
    }

    public function verificaciones() {
        $condicion01 = "" != $this->propertyCode;
        
      $this->errores = [];
        if(!$condicion01){
            $this->errores[] = "No tiene propertyCode  {propertyCode:$this->propertyCode}";
        }
        
        return ["verificacion" => $condicion01, "errores" => $this->errores];           
    }
}