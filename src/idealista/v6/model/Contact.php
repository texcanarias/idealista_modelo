<?php namespace inmotek\idealista\v6\model;

class Contact implements verificaciones {

    public string $contactName = "";
    public string $contactEmail = "";
    public string $contactPrimaryPhonePrefix = "";
    public string $contactPrimaryPhoneNumber = "";
    public string $contactSecondaryPhonePrefix = "";
    public string $contactSecondaryPhoneNumber = "";
    
    use \inmotek\idealista\v6\model\traitLimpieza;
    use \inmotek\idealista\v6\model\traitErrores;

    public function __construct() {
    }
    
    public function setContactName(string $contactName) : self {
        $this->contactName = mb_substr($contactName, 0, 60);
        return $this;
    }

    public function setContactEmail(string $contactEmail) : self {
      $this->contactEmail = "";
        if ("" != $contactEmail) {
            $preg = "/(([a-zA-Z0-9-_\\.])+)@((?:[a-zA-Z0-9-_]+\\.)+)([a-zA-Z]{2,5})/";
            if (preg_match($preg, $contactEmail)) {
                $this->contactEmail = $contactEmail;
            } else {
                $this->setErrores("contactEmail no cumple con su pattern -> " . $contactEmail . " " . $preg);
            }
        }
        return $this;
    }

    public function setContactPrimaryPhonePrefix(string $contactPrimaryPhonePrefix) : self{
        $this->contactPrimaryPhonePrefix = "34";
        if ("" != $contactPrimaryPhonePrefix) {
            $preg = "/[1-9][0-9]{0,2}/";
            if (preg_match($preg, $contactPrimaryPhonePrefix)) {
                $this->contactPrimaryPhonePrefix = $contactPrimaryPhonePrefix;
            } else {
                $this->setErrores("contactPrimaryPhonePrefix no cumple con su pattern -> " . $contactPrimaryPhonePrefix . " " . $preg);
            }
        }
        return $this;
    }

    public function setContactPrimaryPhoneNumber(string $contactPrimaryPhoneNumber) : self {
      $this->contactPrimaryPhoneNumber = "";
        if ("" != $contactPrimaryPhoneNumber) {
            $preg = "/[0-9]{5,12}/";
            if (preg_match($preg, $contactPrimaryPhoneNumber)) {
                $this->contactPrimaryPhoneNumber = $contactPrimaryPhoneNumber;
                if("" == $this->contactPrimaryPhonePrefix){
                    $this->contactPrimaryPhonePrefix = "34";
                }
            } else {
                $this->setErrores("contactPrimaryPhoneNumber no cumple con su pattern -> " . $contactPrimaryPhoneNumber . " " . $preg);
            }
        }
        return $this;
    }

    public function setContactSecondaryPhonePrefix(string $contactSecondaryPhonePrefix) : self{
        $this->contactSecondaryPhonePrefix = "34";
        if ("" != $contactSecondaryPhonePrefix) {
            $preg = "/[1-9][0-9]{0,2}/";
            if (preg_match($preg, $contactSecondaryPhonePrefix)) {
                $this->contactSecondaryPhonePrefix = $contactSecondaryPhonePrefix;
            } else {
                $this->setErrores("contactSecondaryPhonePrefix no cumple con su pattern -> " . $contactSecondaryPhonePrefix . " " . $preg);
            }
        }
        return $this;
    }

    public function setContactSecondaryPhoneNumber(string $contactSecondaryPhoneNumber) : self{
      $this->contactSecondaryPhoneNumber = "";
        if ("" != $contactSecondaryPhoneNumber) {
            $preg = "/[0-9]{5,12}/";
            if (preg_match($preg, $contactSecondaryPhoneNumber)) {
                $this->contactSecondaryPhoneNumber = $contactSecondaryPhoneNumber;
                if("" == $this->contactSecondaryPhonePrefix){
                    $this->contactSecondaryPhonePrefix = 34;
                }
            } else {
                $this->setErrores("contactSecondayPhoneNumber no cumple con su pattern -> " . $contactSecondaryPhoneNumber . " " . $preg);
            }
        }
        return $this;
    }

    public function verificaciones() {
        $verifica01 = !("" == $this->contactPrimaryPhoneNumber && "" != $this->contactPrimaryPhonePrefix);
        $verifica02 = !("" == $this->contactSecondaryPhoneNumber && "" != $this->contactSecondaryPhonePrefix);

        if(!$verifica01){
            $this->errores[] = "No tiene contactPrimaryPhoneNumber y/o contactPrimaryPhonePrefix {contactPrimaryPhoneNumber:$this->contactPrimaryPhoneNumber, contactPrimaryPhonePrefix:$this->contactPrimaryPhonePrefix}";
        }
        if(!$verifica02){
            $this->errores[] = "No tiene contactSecondaryPhoneNumber y/o contactSecondaryPhonePrefix  {contactSecondaryPhoneNumber:$this->contactSecondaryPhoneNumber, contactSecondaryPhonePrefix:$this->contactSecondaryPhonePrefix}";
        }
        
        return ["verificacion" => $verifica01 || $verifica02, "errores" => $this->errores];           
    }

}
