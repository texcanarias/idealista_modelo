<?php namespace inmotek\idealista\v6\model;

trait traitLimpieza {

    /**
     * Elimina de un objeto las propiedades con valor null
     * @return $this stdClass
     */
    public function limpieza(){
        foreach (get_object_vars ($this) as $objectPropertyName => $objectPropertyValue){
            if(null === $objectPropertyValue){
                unset($this->$objectPropertyName);
            }
        }
        return $this;
    }

}