<?php namespace inmotek\idealista\v6\model\feature;

/**
 * Locales
 */
class Premises extends \inmotek\idealista\v6\model\feature\Feature implements \inmotek\idealista\v6\model\verificaciones {

    use traits\featureEnergyCertificate;
    use traits\featureConservation;
    use traits\featureAreaConstructed;
    use traits\featureAreaUsable;
    use traits\featureAreaPlot;
    use traits\featureBathroomNumber;
    use traits\featureFloorsBuilding;
    use traits\featureSecurity;
    
    
    use \inmotek\idealista\v6\model\traitLimpieza;

    public static $TYPE_PREMISES = "premises";
    public static $TYPE_PREMISES_INDUSTRIAL = "premises_industrial";
    public static $TYPE_PREMISES_COMMERCIAL = "premises_commercial";

    public static $UBICATION_SHOPPING = "shopping";
    public static $UBICATION_STREET = "street";
    public static $UBICATION_MEZZANINE = "mezzanine";
    public static $UBICATION_BELOW_GROUND = "belowGround";
    public static $UBICATION_OTHER = "other";
    public static $UBICATION_UNKNOWN = "unknown";


    public ?bool $featuresBathroomAdapted = null;
    public ?bool $featuresConditionedAir = null;
    public ?bool $featuresEquippedKitchen = null;
    public ?int $featuresFacadeArea = null;
    public ?bool $featuresHeating = null;
    public ?string $featuresLastActivity = null;
    public ?bool $featuresLocatedAtCorner = null;
    public ?int $featuresRooms = null;
    public ?bool $featuresSecuritySystem = null;
    public ?bool $featuresStorage = null;
    public ?bool $featuresSmokeExtraction = null;
    public ?string $featuresUbication = null; //["shopping","street","mezzanine","belowGround","other","unknown"]
    public ?int $featuresWindowsNumber = null;

    public function setFeaturesType(?string $featuresType) : self{
        $this->featuresType = $featuresType;
        return $this;
    }

    /**
     * availability of adapted bathroom for disabled people
     * @param boolean $featuresBathroomAdapted
     * @return $this
     */
    public function setFeaturesBathroomAdapted(?bool $featuresBathroomAdapted) : self{
        $this->featuresBathroomAdapted = $featuresBathroomAdapted;
        return $this;
    }

    /**
     * has air conditioning
     * @param boolean $featuresConditionedAir
     * @return $this
     */
    public function setFeaturesConditionedAir(?bool $featuresConditionedAir) :self {
        $this->featuresConditionedAir = $featuresConditionedAir;
        return $this;
    }

    /**
     * equipped with kitchen only
     * @param boolean $featuresEquippedKitchen
     * @return $this
     */
    public function setFeaturesEquippedKitchen(?bool $featuresEquippedKitchen) : self {
        $this->featuresEquippedKitchen = $featuresEquippedKitchen;
        return $this;
    }

    /**
     * facade area
     * @param integer $featuresFacadeArea
     * @return $this
     */
    public function setFeaturesFacadeArea(?int $featuresFacadeArea) : self{
        if (null != $featuresFacadeArea) {
            if (1 <= $featuresFacadeArea && 99 >= $featuresFacadeArea) {
                $this->featuresFacadeArea = (int)$featuresFacadeArea;
            } else {
                $this->setErrores("featuresFacadeArea debe estar entre 1 y 99 y es $featuresFacadeArea");
            }
        }
        return $this;
    }

    /**
     * heating availability
     * @param boolean $featuresHeating
     * @return $this
     */
    public function setFeaturesHeating(?bool $featuresHeating) : self{
        $this->featuresHeating = $featuresHeating;
        return $this;
    }

    /**
     * last activity
     * @param integer $featuresLastActivity
     * @return $this
     */
    public function setFeaturesLastActivity(?string $featuresLastActivity) : self{
        if(null != $featuresLastActivity){
            $this->featuresLastActivity = mb_substr($featuresLastActivity, 0, 100);
        }
        return $this;
    }

    /**
     * located at corner
     * @param boolean $featuresLocatedAtCorner
     * @return $this
     */
    public function setFeaturesLocatedAtCorner(?bool $featuresLocatedAtCorner) : self{
        $this->featuresLocatedAtCorner = $featuresLocatedAtCorner;
        return $this;
    }

    /**
     * 
     * @param type $featuresRooms
     * @return $this
     */
    public function setFeaturesRooms(?int $featuresRooms) : self{
        if (null != $featuresRooms) {
            if (1 <= $featuresRooms && 99 >= $featuresRooms) {
                $this->featuresRooms = (int)$featuresRooms;
            } else {
                $this->setErrores("featuresRooms debe estar entre 1 y 99 y es $featuresRooms");
            }
        }
        return $this;
    }

    /**
     * security system
     * @param boolean $featuresSecuritySystem
     * @return $this
     */
    public function setFeaturesSecuritySystem(?bool $featuresSecuritySystem) : self {
        $this->featuresSecuritySystem = $featuresSecuritySystem;
        return $this;
    }

    /**
     * storage room
     * @param boolean $featuresStorage
     * @return $this
     */
    public function setFeaturesStorage(?bool $featuresStorage) : self{
        $this->featuresStorage = $featuresStorage;
        return $this;
    }

    /**
     * smoke extraction
     * @param boolean $featuresSmokeExtraction
     * @return $this
     */
    public function setFeaturesSmokeExtraction(?bool $featuresSmokeExtraction) : self{
        $this->featuresSmokeExtraction = $featuresSmokeExtraction;
        return $this;
    }

    /**
     * ubication
     * @param string $featuresUbication
     * @return $this
     */
    public function setFeaturesUbication(?string $featuresUbication) : self{
        $this->featuresUbication = $featuresUbication;
        return $this;
    }

    public function setFeaturesWindowsNumber(?int $featuresWindowsNumber) : self{
        if (null != $featuresWindowsNumber) {
            if (1 <= $featuresWindowsNumber && 999 >= $featuresWindowsNumber) {
                $this->featuresWindowsNumber = (int)$featuresWindowsNumber;
            } else {
                $this->setErrores("featuresWindowsNumber debe estar entre 1 y 999 y es $featuresWindowsNumber");
            }
        }
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->featuresType && "" != $this->featuresAreaConstructed;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene featuresType y/o featuresAreaConstructed  {featuresType:$this->featuresType, featuresAreaConstructed:$this->featuresAreaConstructed}";
        }
        
        return ["verificacion" => $verificacion01, "errores" => $this->errores];                
    }

}
