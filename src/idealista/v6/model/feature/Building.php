<?php namespace inmotek\idealista\v6\model\feature;

class Building extends \inmotek\idealista\v6\model\feature\Feature implements \inmotek\idealista\v6\model\verificaciones {

    use traits\featureEnergyCertificate;
    use traits\featureConservation;
    use traits\featureBuiltYear;
    use traits\featureAreaConstructed;
    use traits\featureAreaTradableMinimun;
    use traits\featureClassification;
    use traits\featureFloorsBuilding;
    
    use \inmotek\idealista\v6\model\traitLimpieza;

    static public string $TYPE_BUILDING = "building";

    public ?int $featuresBuiltProperties = null;
    public ?int $featuresLiftNumber = null;
    public ?int $featuresParkingSpacesNumber = null;
    public ?bool $featuresPropertyTenants = null;
    public ?bool $featuresSecurityPersonnel = null;

    public function __construct() {
        $this->featuresType = self::$TYPE_BUILDING;
    }

    /**
     * 
     * @param integer $featuresBuiltProperties
     * @return $this
     * @throws \Exception
     */
    public function setFeaturesBuiltProperties(?int $featuresBuiltProperties) : self{
        if (null != $featuresBuiltProperties) {
            if (1 <= $featuresBuiltProperties && 999999 >= $featuresBuiltProperties) {
                $this->featuresBuiltProperties = (int)$featuresBuiltProperties;
            } else {
                $this->setErrores("featuresBuiltProperties debe estar entre 1 y 999999 y es $featuresBuiltProperties");
            }
        }
        return $this;
    }

    /**
     * lifts number
     * @param integer $featuresLiftNumber
     * @return $this
     */
    public function setFeaturesLiftNumber(?int $featuresLiftNumber) : self{
        if (null != $featuresLiftNumber) {
            if (1 <= $featuresLiftNumber && 9 >= $featuresLiftNumber) {
                $this->featuresLiftNumber = (int)$featuresLiftNumber;
            } else {
                $this->setErrores("featuresLiftNumber debe estar entre 1 y 9 y es $featuresLiftNumber");
            }
        }
        return $this;
    }

    public function setFeaturesParkingSpacesNumber(?int $featuresParkingSpacesNumber) : self{
        if (null != $featuresParkingSpacesNumber) {
            if (1 <= $featuresParkingSpacesNumber && 99 >= $featuresParkingSpacesNumber) {
                $this->featuresParkingSpacesNumber = (int)$featuresParkingSpacesNumber;
            } else {
                $this->setErrores("featuresParkingSpacesNumber debe estar entre 1 y 99 y es $featuresParkingSpacesNumber");
            }
        }
        return $this;
    }

    /**
     * Inquilinos
     * @param boolean $featuresPropertyTenants
     * @return $this
     */
    public function setFeaturesPropertyTenants(?bool $featuresPropertyTenants) : self{
        $this->featuresPropertyTenants = $featuresPropertyTenants;
        return $this;
    }

    /**
     * security personnel - personal de seguridad
     * @param boolean $featuresSecurityPersonnel
     * @return $this
     */
    public function setFeaturesSecurityPersonnel(?bool $featuresSecurityPersonnel) : self{
        $this->featuresSecurityPersonnel = $featuresSecurityPersonnel;
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->featuresType && "" != $this->featuresAreaConstructed;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene featuresType y featuresAreaConstructed {featuresType:$this->featuresType, featuresAreaConstructed:$this->featuresAreaConstructed}";
        }
        
        return ["verificacion" => $verificacion01, "errores" => $this->errores];
    }

}
