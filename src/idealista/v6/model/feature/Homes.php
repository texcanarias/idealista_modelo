<?php namespace inmotek\idealista\v6\model\feature;

class Homes extends \inmotek\idealista\v6\model\feature\Feature implements \inmotek\idealista\v6\model\verificaciones {

    use traits\featureEnergyCertificate;
    use traits\featureConservation;
    use traits\featureOrientation;
    use traits\featureBuiltYear;
    use traits\featureAreaConstructed;
    use traits\featureAreaPlot;
    use traits\featureBathroomNumber;
    use traits\featureFloorsBuilding;
    use traits\featureWindowsLocation;
    
    use \inmotek\idealista\v6\model\traitLimpieza;

    static string $TYPE_FLAT = "flat";
    static string $TYPE_HOUSE ="house";
    static string $TYPE_HOUSE_INDEPENDENT = "house_independent";
    static string $TYPE_SEMIDETACHED = "house_semidetached";
    static string $TYPE_HOUSE_TERRACED = "house_terraced";
    static string $TYPE_HOUSE_VILLA = "house_villa";
    static string $TYPE_RUSTIC = "rustic";
    static string $TYPE_RUSTIC_HOUSE = "rustic_house";
    static string $TYPE_RUSTIC_VILLAGE = "rustic_village";
    static string $TYPE_RUSTIC_CASTLE = "rustic_castle";
    static string $TYPE_RUSTIC_PALACE = "rustic_palace";
    static string $TYPE_RUSTIC_RURAL = "rustic_rural";
    static string $TYPE_RUSTIC_CASERON = "rustic_caseron";
    static string $TYPE_RUSTIC_TORRE = "rustic_torre";
    static string $TYPE_CORTIJO = "rustic_cortijo";
    static string $TYPE_MASIA = "rustic_masia";
    static string $TYPE_TERRERA = "rustic_terrera";
    

    static string $HEATING_TYPE_CENTRAL_GAS = "centralGas";
    static string $HEATING_TYPE_CENTRAL_FUELOIL = "centralFuelOil";
    static string $HEATING_TYPE_CENTRAL_OTHER = "centralOther";
    static string $HEATING_TYPE_INDIVIDUAL_GAS = "individualGas";
    static string $HEATING_TYPE_INDIVIDUAL_PROPANE_BUTANE = "individualPropaneButane";
    static string $HEATING_TYPE_INDIVIDUAL_ELECTRIC = "individualElectric";
    static string $HEATING_TYPE_INDIVIDUAL_AIRCONDITION_HEAT_PUMP = "individualAirConditioningHeatPump";
    static string $HEATING_TYPE_INDIVIDUAL_OTHER = "individualOther";
    static string $HEATING_TYPE_INDIVIDUAL_NOHEATING = "noHeating";


    public ?int $featuresAreaUsable = null;
    public ?bool $featuresBalcony = null;
    public ?int $featuresBedroomNumber = null;
    public ?bool $featuresConditionedAir = null;
    public ?bool $featuresChimney = null;
    public ?bool $featuresDoorman = null;
    public ?bool $featuresDuplex = null;
    public ?bool $featuresEmergencyExit = null;
    public ?bool $featuresEmergencyLights = null;
    public ?bool $featuresEquippedKitchen = null;
    public ?bool $featuresEquippedWithFurniture = null;
    public ?bool $featuresFloorsInTop = null;
    public ?bool $featuresGarden = null;
    public ?bool $featuresHandicapAdaptedAccess = null;
    public ?bool $featuresHandicapAdaptedUse = null;
    public ?bool $featuresLiftAvailable = null;
    public ?bool $featuresParkingAvailable = null;
    public ?bool $featuresPenthouse = null;
    public ?bool $featuresPetsAllowed = null;
    public ?bool $featuresPool = null;
    public ?int $featuresRooms = null;
    public ?bool $featuresStorage = null;
    public ?bool $featuresStudio = null;
    public ?bool $featuresTerrace = null;
    public ?bool $featuresWardrobes = null;
    public ?string $featuresHeatingType = null;

    /**
     * //["flat","house","house_independent","house_semidetached","house_terraced","rustic","rustic_house","rustic_village","rustic_castle","rustic_palace","rustic_rural","rustic_caseron","rustic_cortijo","rustic_masia","rustic_terrera","rustic_torre"]
     * @param type $featuresType
     * @return $this
     */
    public function setFeaturesType(?string $featuresType)  : self{
        $this->featuresType = $featuresType;
        return $this;
    }


    /**
     * 
     * @param integer $featuresAreaUsable
     * @return $this
     * @throws \Exception
     */
    public function setFeaturesAreaUsable(?int $featuresAreaUsable) : self{
        if (null != $featuresAreaUsable) {
            if (1 <= $featuresAreaUsable && 99999 >= $featuresAreaUsable) {
                $this->featuresAreaUsable = (int)$featuresAreaUsable;
            } else {
                $this->setErrores("featuresAreaUsable debe estar entre 1 y 99999 y es $featuresAreaUsable");
            }
        }
        return $this;
    }

    /**
     * 
     * @param boolean $featuresBalcony
     * @return $this
     */
    public function setFeaturesBalcony(?bool $featuresBalcony) : self{
        $this->featuresBalcony = $featuresBalcony;
        return $this;
    }

    /**
     * 
     * @param integer $featuresBedroomNumber
     * @return $this
     */
    public function setFeaturesBedroomNumber(?int $featuresBedroomNumber) : self{
        if (null != $featuresBedroomNumber) {
            if (1 <= $featuresBedroomNumber && 99 >= $featuresBedroomNumber) {
                $this->featuresBedroomNumber = (int)$featuresBedroomNumber;
            } else {
                $this->setErrores("featuresBedroomNumber debe estar entre 1 y 99 y es $featuresBedroomNumber");
            }
        }
        return $this;
    }

    /**
     * 
     * @param boolean $featuresConditionedAir
     * @return $this
     */
    public function setFeaturesConditionedAir(?bool $featuresConditionedAir) : self{
        $this->featuresConditionedAir = $featuresConditionedAir;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresChimney
     * @return $this
     */
    public function setFeaturesChimney(?bool $featuresChimney) : self{
        $this->featuresChimney = $featuresChimney;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresDoorman
     * @return $this
     */
    public function setFeaturesDoorman(?bool $featuresDoorman) : self {
        if(null != $featuresDoorman){
            $this->featuresDoorman = $featuresDoorman;
        }
        return $this;
    }

    /**
     * 
     * @param boolean $featuresDuplex
     * @return $this
     */
    public function setFeaturesDuplex(?bool $featuresDuplex) :self {
        $this->featuresDuplex = $featuresDuplex;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresEmergencyExit
     * @return $this
     */
    public function setFeaturesEmergencyExit(?bool $featuresEmergencyExit) : self{
        $this->featuresEmergencyExit = $featuresEmergencyExit;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresEmergencyLights
     * @return $this
     */
    public function setFeaturesEmergencyLights(?bool $featuresEmergencyLights) : self{
        $this->featuresEmergencyLights = $featuresEmergencyLights;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresEquippedKitchen
     * @return $this
     */
    public function setFeaturesEquippedKitchen(?bool $featuresEquippedKitchen) : self{
        $this->featuresEquippedKitchen = $featuresEquippedKitchen;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresEquippedWithFurniture
     * @return $this
     */
    public function setFeaturesEquippedWithFurniture(?bool $featuresEquippedWithFurniture) : self{
        $this->featuresEquippedWithFurniture = $featuresEquippedWithFurniture;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresFloorsInTop
     * @return $this
     */
    public function setFeaturesFloorsInTop(?bool $featuresFloorsInTop) : self{
        $this->featuresFloorsInTop =$featuresFloorsInTop;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresGarden
     * @return $this
     */
    public function setFeaturesGarden(?bool $featuresGarden) : self{
        $this->featuresGarden = $featuresGarden;
        return $this;
    }

    /**
     * access adapted to people with reduced mobility
     * @param boolean $featuresHandicapAdaptedAccess
     * @return $this
     */
    public function setFeaturesHandicapAdaptedAccess(?bool $featuresHandicapAdaptedAccess) : self{
        $this->featuresHandicapAdaptedAccess = $featuresHandicapAdaptedAccess;
        return $this;
    }

    /**
     * property adapted to people with reduced mobility
     * @param boolean $featuresHandicapAdaptedUse
     * @return $this
     */
    public function setFeaturesHandicapAdaptedUse(?bool $featuresHandicapAdaptedUse) : self{
        $this->featuresHandicapAdaptedUse = $featuresHandicapAdaptedUse;
        return $this;
    }

    /**
     * only for flat type
     * @param boolean $featuresLiftAvailable
     * @return $this
     */
    public function setFeaturesLiftAvailable(?bool $featuresLiftAvailable) : self{
        $this->featuresLiftAvailable = $featuresLiftAvailable;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresParkingAvailable
     * @return $this
     */
    public function setFeaturesParkingAvailable(?bool $featuresParkingAvailable) : self{
        $this->featuresParkingAvailable = boolval($featuresParkingAvailable);
        return $this;
    }

    /**
     * flat on the top floor
     * @param boolean $featuresPenthouse
     * @return $this
     */
    public function setFeaturesPenthouse(?bool $featuresPenthouse) : self{
        $this->featuresPenthouse = $featuresPenthouse;
        return $this;
    }

    /**
     * pet friendly flat
     * @param boolean $featuresPetsAllowed
     * @return $this
     */
    public function setFeaturesPetsAllowed(?bool $featuresPetsAllowed) : self{
        $this->featuresPetsAllowed = $featuresPetsAllowed;
        return $this;
    }

    /**
     * pool availability
     * @param boolean $featuresPool
     * @return $this
     */
    public function setFeaturesPool(?bool $featuresPool) : self{
        $this->featuresPool = $featuresPool;
        return $this;
    }

    /**
     * rooms number
     * @param integer $featuresRooms
     * @return $this
     */
    public function setFeaturesRooms(?int $featuresRooms) : self{
        if (null != $featuresRooms) {
            if (1 <= $featuresRooms && 99 >= $featuresRooms) {
                $this->featuresRooms = (int)$featuresRooms;
            } else {
                $this->setErrores("featuresRooms debe estar entre 1 y 99 y es $featuresRooms");
            }
        }
        return $this;        
    }

    /**
     * storage room
     * @param boolean $featuresStorage
     * @return $this
     */
    public function setFeaturesStorage(?bool $featuresStorage) :self{
        $this->featuresStorage = $featuresStorage;
        return $this;
    }

    /**
     * flat with a single room
     * @param boolean $featuresStudio
     * @return $this
     */
    public function setFeaturesStudio(?bool $featuresStudio) : self{
        $this->featuresStudio = $featuresStudio;
        return $this;
    }

    /**
     * terrace
     * @param boolean $featuresTerrace
     * @return $this
     */
    public function setFeaturesTerrace(?bool $featuresTerrace) : self{
        $this->featuresTerrace = $featuresTerrace;
        return $this;
    }

    /**
     * wardrobes availability
     * @param boolean $featuresWardrobes
     * @return $this
     */
    public function setFeaturesWardrobes(?bool $featuresWardrobes) : self{
        $this->featuresWardrobes = $featuresWardrobes;
        return $this;
    }

    /**
     * heatingType ["centralGas","centralFuelOil","centralOther","individualGas","individualPropaneButane","individualElectric","individualAirConditioningHeatPump","individualOther","noHeating"]
     * @param type $featuresHeatingType
     * @return $this
     */
    public function setFeaturesHeatingType(?string $featuresHeatingType) : self{
        $this->featuresHeatingType = $featuresHeatingType;
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->featuresType && "" != $this->featuresAreaConstructed && "" != $this->featuresBathroomNumber;
        $verificacion02 = "" != $this->featuresRooms;
        $verificacion03 = "" != $this->featuresBedroomNumber;

        if(!$verificacion01){
            $this->errores[] = "No tiene featuresType y/o featuresAreaConstructed y/o featuresBathroomNumber {featuresType:$this->featuresType, featuresBathroomNumber:$this->featuresBathroomNumber}";
        }
        if(!$verificacion02 || !$verificacion03){
            $this->errores[] = "No tiene featuresRooms o featuresBedroomNumber {featuresRooms:$this->featuresRooms, featuresBedroomNumber:$this->featuresBedroomNumber}";
        }
        
        return ["verificacion" => ($verificacion01 && ($verificacion02 || $verificacion03)), "errores" => $this->errores];
    }

}
