<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureAccessType{
    public ?string $featuresAccessType = null;

    public static $ACCESS_TYPE_URBAN = "urban";
    public static $ACCESS_TYPE_ROAD = "road";
    public static $ACCESS_TYPE_TRACK = "track";
    public static $ACCESS_TYPE_HIGHWAY = "highway";
    public static $ACCESS_TYPE_UNKNOWN = "unknown";


    /**
     * //["urban","road","track","highway","unknown"]
     * @param type $featuresConservation
     * @return $this
     */
    public function setFeaturesAccessType(?string $featuresAccessType) : self{
        if(null != $featuresAccessType){
            $this->featuresAccessType = $featuresAccessType;
        }
        return $this;
    }


}