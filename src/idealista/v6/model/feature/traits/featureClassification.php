<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureClassification {

    public ?bool $featuresClassificationChalet = null;
    public ?bool $featuresClassificationCommercial = null;
    public ?bool $featuresClassificationHotel = null;
    public ?bool $featuresClassificationIndustrial = null;
    public ?bool $featuresClassificationOffice = null;
    public ?bool $featuresClassificationOther = null;

    /**
     * classified for residential house (detached) - clasificado para casa residencial (individual)
     * @param boolean $featuresClassificationChalet
     * @return $this
     */
    public function setFeaturesClassificationChalet(?bool $featuresClassificationChalet) : self{
        $this->featuresClassificationChalet = $featuresClassificationChalet;
        return $this;
    }

    /**
     * chalet classification ,classified for tertiary commercial properties - clasificado para propiedades comerciales terciarias
     * @param boolean $featuresClassificationCommercial
     * @return $this
     */
    public function setFeaturesClassificationCommercial(?bool $featuresClassificationCommercial) : self{
        $this->featuresClassificationCommercial = $featuresClassificationCommercial;
        return $this;
    }

    /**
     * hotel classification, classified for hotels properties
     * @param boolean $featuresClassificationHotel
     * @return $this
     */
    public function setFeaturesClassificationHotel(?bool $featuresClassificationHotel) : self{
        $this->featuresClassificationHotel = $featuresClassificationHotel;
        return $this;
    }

    /**
     * industrial classification - classified for industrial properties
     * @param boolean $featuresClassificationIndustrial
     * @return $this
     */
    public function setFeaturesClassificationIndustrial(?bool $featuresClassificationIndustrial) : self{
        $this->featuresClassificationIndustrial = $featuresClassificationIndustrial;
        return $this;
    }

    /**
     * office classification
     * @param boolean $featuresClassificationOffice
     * @return $this
     */
    public function setFeaturesClassificationOffice(?bool $featuresClassificationOffice) : self{
        $this->featuresClassificationOffice = $featuresClassificationOffice;
        return $this;
    }

    /**
     * other classification
     * @param boolean $featuresClassificationOther
     * @return $this
     */
    public function setFeaturesClassificationOther(?bool $featuresClassificationOther) : self{
        $this->featuresClassificationOther = $featuresClassificationOther;
        return $this;
    }

}
