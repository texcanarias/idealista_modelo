<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureAreaTradableMinimun {

    public ?int $featuresAreaTradableMinimum = null;

    /**
     * minimum tradable area - área mínima negociable
     * @param type $featuresAreaTradableMinimum
     * @return $this
     */
    public function setFeaturesAreaTradableMinimum(int $featuresAreaTradableMinimum) : self{
        if (null != $featuresAreaTradableMinimum) {
            if (1 <= $featuresAreaTradableMinimum && 999999 >= $featuresAreaTradableMinimum) {
                $this->featuresAreaTradableMinimum = (int)$featuresAreaTradableMinimum;
            } else {
                $this->setErrores("featuresAreaTradableMinimum debe estar entre 1 y 999999 y es $featuresAreaTradableMinimum");
            }
        }
        return $this;
    }

}
