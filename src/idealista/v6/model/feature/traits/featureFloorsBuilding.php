<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureFloorsBuilding {

    public ?int $featuresFloorsBuilding = null;

    /**
     * 
     * @param integer $featuresFloorsBuilding
     * @return $this
     * @throws \Exception
     */
    public function setFeaturesFloorsBuilding(?int $featuresFloorsBuilding) : self{
        if (null != $featuresFloorsBuilding) {
            if (1 <= $featuresFloorsBuilding && 99 >= $featuresFloorsBuilding) {
                $this->featuresFloorsBuilding = (int)$featuresFloorsBuilding;
            } else {
                $this->setErrores("featuresFloorsBuilding debe estar entre 1 y 99 y es $featuresFloorsBuilding");
            }
        }
        return $this;
    }

}
