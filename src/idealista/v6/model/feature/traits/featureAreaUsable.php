<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureAreaUsable {

    public ?int $featuresAreaAreaUsable = null;

    /**
     * functional area
     * @param integer $featuresAreaUsable
     * @return $this
     */    
    public function setFeaturesAreaUsable(?int $featuresAreaUsable) : self{
        if (null != $featuresAreaUsable) {
            if (1 <= $featuresAreaUsable && 99999 >= $featuresAreaUsable) {
                $this->featuresAreaUsable = intval($featuresAreaUsable);
            } else {
                $this->setErrores("featuresAreaUsable debe estar entre 1 y 99999 y es $featuresAreaUsable");
            }
        }
        return $this;
    }


}
