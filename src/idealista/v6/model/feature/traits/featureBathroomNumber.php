<?php

namespace inmotek\idealista\v6\model\feature\traits;

trait featureBathroomNumber
{

    public ?int $featuresBathroomNumber = null;

    /**
     * Número de baños
     * @param integer $featuresBathroomNumber
     * @return $this
     */
    public function setFeaturesBathroomNumber(?int $featuresBathroomNumber): self
    {
        if (null != $featuresBathroomNumber) {
            if ("" != $featuresBathroomNumber && 0 != $featuresBathroomNumber) {
                if (1 <= $featuresBathroomNumber && 99 >= $featuresBathroomNumber) {
                    $this->featuresBathroomNumber = (int) $featuresBathroomNumber;
                } else {
                    $this->setErrores("featuresBathroomNumber debe estar entre 1 y 99 y es $featuresBathroomNumber");
                }
            }
        }
        return $this;
    }
}
