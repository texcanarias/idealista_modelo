<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureBuiltYear {

    public ?int $featuresBuiltYear = null;

    public function setFeaturesBuiltYear(?int $featuresBuiltYear)  : self{
        if(null != $featuresBuiltYear){
            if ("" != $featuresBuiltYear) {
                if (1700 <= $featuresBuiltYear && 2100 >= $featuresBuiltYear) {
                    $this->featuresBuiltYear = (int)$featuresBuiltYear;
                } else {
                    $this->setErrores("featuresBuiltYear debe estar entre 1700 y 2100 y es $featuresBuiltYear");
                }
            }
        }
        return $this;
    }

}
