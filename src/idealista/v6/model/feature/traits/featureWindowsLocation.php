<?php namespace inmotek\idealista\v6\model\feature\traits;

trait featureWindowsLocation {

    static public string $EXTERIOR = "external";
    static public string $INTERIOR = "internal";

    public ?string $featuresWindowsLocation;

    /**
     * windows location - internal / external flat based on windows view. Only available for Spain
     * @param type $featuresWindowsLocation
     * @return $this
     */
    public function setFeaturesWindowsLocation(?string $featuresWindowsLocation) : self{
        $this->featuresWindowsLocation = $featuresWindowsLocation;
        return $this;
    }

}
