<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureAreaPlot {

    public ?int $featuresAreaPlot = null;

    /**
     * plot area - plot square meters (área de la parcela)
     * @param integer $featuresAreaPlot
     * @return $this
     * @throws \Exception
     */
    public function setFeaturesAreaPlot(?int $featuresAreaPlot) : self{
        if (null != $featuresAreaPlot) {
            if (1 <= $featuresAreaPlot && 99999999 >= $featuresAreaPlot) {
                $this->featuresAreaPlot = (int) $featuresAreaPlot;
            } else {
                $this->setErrores("featuresAreaPlot debe estar entre 1 y 99999999 y es $featuresAreaPlot");
            }
        }
        return $this;
    }

}
