<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureConservation{
    static $CONSERVATION_TO_RESTORE = "toRestore";
    static $CONSERVATION_GOOD = "good";
    static $CONSERVATION_NEW = "new";


    public ?string $featuresConservation = null;

    /**
     * //["new","good","toRestore"]
     * @param type $featuresConservation
     * @return $this
     */
    public function setFeaturesConservation(?string $featuresConservation) : self{
        $this->featuresConservation = $featuresConservation;
        return $this;
    }


}