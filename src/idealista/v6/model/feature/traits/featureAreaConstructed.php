<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureAreaConstructed {

    public ?int $featuresAreaConstructed = null;

    /**
     * constructed area - built square meters
     * @param integer $featuresAreaConstructed
     * @return $this
     */
    public function setFeaturesAreaConstructed(?int $featuresAreaConstructed) : self{
        if (null != $featuresAreaConstructed) {
            if (1 <= $featuresAreaConstructed && 99999 >= $featuresAreaConstructed) {
                $this->featuresAreaConstructed = (int)$featuresAreaConstructed;
            } else {
                $this->setErrores("featuresAreaConstructed debe estar entre 1 y 99999 y es $featuresAreaConstructed");
            }
        }
        return $this;
    }

}
