<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureOrientation{

    
    public ?bool $featuresOrientationNorth = null;
    public ?bool $featuresOrientationSouth = null;
    public ?bool $featuresOrientationWest = null;
    public ?bool $featuresOrientationEast = null;

    /**
     * 
     * @param boolean $featuresOrientationNorth
     * @return $this
     */
    public function setFeaturesOrientationNorth(?bool $featuresOrientationNorth) : self{
        $this->featuresOrientationNorth = $featuresOrientationNorth;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresOrientationSouth
     * @return $this
     */
    public function setFeaturesOrientationSouth(?bool $featuresOrientationSouth) : self {
        $this->featuresOrientationSouth = $featuresOrientationSouth;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresOrientationWest
     * @return $this
     */
    public function setFeaturesOrientationWest(?bool $featuresOrientationWest) : self {
        $this->featuresOrientationWest = $featuresOrientationWest;
        return $this;
    }

    /**
     * 
     * @param boolean $featuresOrientationEast
     * @return $this
     */
    public function setFeaturesOrientationEast(?bool $featuresOrientationEast)  : self{
        $this->featuresOrientationEast = $featuresOrientationEast;
        return $this;
    }


}