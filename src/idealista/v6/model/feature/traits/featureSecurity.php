<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureSecurity{

    public ?bool $featuresSecurityAlarm = null;
    public ?bool $featuresSecurityDoor = null;


    /**
     * security door
     * @param boolean $featuresSecurityDoor
     * @return $this
     */
    public function setFeaturesSecurityDoor(?bool $featuresSecurityDoor)  : self{
        $this->featuresSecurityDoor = $featuresSecurityDoor;
        return $this;
    }
    
    /**
     * security alarm system
     * @param boolean $featuresSecurityAlarm
     * @return $this
     */
    public function setFeaturesSecurityAlarm(?bool $featuresSecurityAlarm)  : self{
        $this->featuresSecurityAlarm = $featuresSecurityAlarm;
        return $this;
    }


}