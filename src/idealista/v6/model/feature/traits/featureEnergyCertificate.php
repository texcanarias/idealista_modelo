<?php 
namespace inmotek\idealista\v6\model\feature\traits;

trait featureEnergyCertificate{
    public $featuresEnergyCertificateRating;
    public $featuresEnergyCertificatePerformance;
    public $featuresEnergyCertificateType;
 
    static public $RATING_A = "A";
    static public $RATING_B = "B";
    static public $RATING_C = "C";
    static public $RATING_D = "D";
    static public $RATING_E = "E";
    static public $RATING_F = "F";
    static public $RATING_G = "G";
    static public $RATING_EXEMPT = "exempt";
    static public $RATING_IN_PROCESS = "inProcess";
    static public $RATING_UNKNOWN = "unknown";

    static public ?string $TYPE_PROJECT = "project";
    static public ?string $TYPE_COMPLETED = "completed";

    /**
     * @param type $featuresEnergyCertificateRating
     * @return $this
     */
    public function setFeaturesEnergyCertificateRating(?string $featuresEnergyCertificateRating) : self{
        $this->featuresEnergyCertificateRating = $featuresEnergyCertificateRating;
        return $this;
    }

    public function setFeaturesEnergyCertificatePerformance(?string $featuresEnergyCertificatePerformance) : self{
        $this->featuresEnergyCertificatePerformance = $featuresEnergyCertificatePerformance;
        return $this;
    }

    /**
     * //["project","completed"]
     * @param type $featuresEnergyCertificateType
     * @return $this
     */
    public function setFeaturesEnergyCertificateType(?string $featuresEnergyCertificateType) : self{
        $this->featuresEnergyCertificateType = $featuresEnergyCertificateType;
        return $this;
    }

}