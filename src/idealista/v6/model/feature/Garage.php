<?php namespace inmotek\idealista\v6\model\feature;

class Garage extends \inmotek\idealista\v6\model\feature\Feature implements \inmotek\idealista\v6\model\verificaciones {

    use traits\featureAreaConstructed;
    use \inmotek\idealista\v6\model\traitLimpieza;
    
    static public string $TYPE_GARAGE = "garage";

    public ?bool $featuresLiftAvailable = null;
    public ?bool $featuresParkingAutomaticDoor = null;
    public ?bool $featuresParkingPlaceCovered = null;
    public ?bool $featuresSecurityAlarm = null;
    public ?bool $featuresSecurityPersonnel = null;
    public ?bool $featuresSecuritySystem = null;

    public function __construct() {
        $this->featuresType =self::$TYPE_GARAGE;
    }

    /**
     * lift
     * @param boolean $featuresLiftAvailable
     * @return $this
     */
    public function setFeaturesLiftAvailable(?bool $featuresLiftAvailable)  : self{
        $this->featuresLiftAvailable = $featuresLiftAvailable;
        return $this;
    }

    /**
     * automatic door
     * @param boolean $featuresParkingAutomaticDoor
     * @return $this
     */
    public function setFeaturesParkingAutomaticDoor(?bool $featuresParkingAutomaticDoor)  : self{
        $this->featuresParkingAutomaticDoor = $featuresParkingAutomaticDoor;
        return $this;
    }

    /**
     * parking covered
     * @param boolean $featuresParkingPlaceCovered
     * @return $this
     */
    public function setFeaturesParkingPlaceCovered(?bool $featuresParkingPlaceCovered)  : self{
        $this->featuresParkingPlaceCovered = $featuresParkingPlaceCovered;
        return $this;
    }

    /**
     * security alarm system
     * @param boolean $featuresSecurityAlarm
     * @return $this
     */
    public function setFeaturesSecurityAlarm(?bool $featuresSecurityAlarm)  : self{
        $this->featuresSecurityAlarm = $featuresSecurityAlarm;
        return $this;
    }

    /**
     * security personnel
     * @param boolean $featuresSecurityPersonnel
     * @return $this
     */
    public function setFeaturesSecurityPersonnel(?bool $featuresSecurityPersonnel)  : self{
        $this->featuresSecurityPersonnel = $featuresSecurityPersonnel;
        return $this;
    }

    /**
     * security system
     * @param boolean $featuresSecuritySystem
     * @return $this
     */
    public function setFeaturesSecuritySystem(?bool $featuresSecuritySystem)  : self{
        $this->featuresSecuritySystem = $featuresSecuritySystem;
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->featuresType && "" != $this->featuresAreaConstructed;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene featuresType y/o featuresAreaConstructed {featuresType:$this->featuresType, featuresAreaConstructed:$this->featuresAreaConstructed}";
        }
        
        return ["verificacion" => $verificacion01, "errores" => $this->errores];
    }
}
