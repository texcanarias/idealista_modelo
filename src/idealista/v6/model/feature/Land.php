<?php namespace inmotek\idealista\v6\model\feature;

class Land extends \inmotek\idealista\v6\model\feature\Feature implements \inmotek\idealista\v6\model\verificaciones {

    use traits\featureClassification;
    use traits\featureAreaPlot;
    use traits\featureAccessType;    
    use \inmotek\idealista\v6\model\traitLimpieza;

    static string $TYPE_LAND = "land";        
    static string $TYPE_LAND_URBAN = "land_urban";
    static string $TYPE_LAND_COUNTRY_BUILDABLE = "land_countrybuildable";
    static string $TYPE_LAND_COUNTRY_NONBUILDABLE = "land_countrynonbuildable";


    public ?int $featuresAreaBuildable = null;
    public ?bool $featuresClassificationBlocks = null;
    public ?string $featuresClassificationPublic = null;
    public ?int $featuresFloorsBuildable = null;
    public ?int $featuresNearestLocationKm = null;
    public ?bool $featuresUtilitiesElectricity = null;
    public ?bool $featuresUtilitiesNaturalGas = null;
    public ?bool $featuresUtilitiesRoadAccess = null;
    public ?bool $featuresUtilitiesSewerage = null;
    public ?bool $featuresUtilitiesSidewalk = null;
    public ?bool $featuresUtilitiesStreetLighting = null;
    public ?bool $featuresUtilitiesWater = null;

    /**
     * ["land","land_urban","land_countrybuildable","land_countrynonbuildable"]
     * @param type $featuresType
     * @return $this
     */
    /*public function setFeaturesType(string $featuresType) {
        $this->featuresType = $featuresType;
        return $this;
    }*/

    public function setFeaturesAreaBuildable(?int $featuresAreaBuildable) : self{
        if (null != $featuresAreaBuildable) {
            if (1 <= $featuresAreaBuildable && 99999 >= $featuresAreaBuildable) {
                $this->featuresAreaBuildable = (int)$featuresAreaBuildable;
            } else {
                $this->setErrores("featuresAreaBuildable debe estar entre 1 y 99999 y es $featuresAreaBuildable");
            }
        }
        return $this;
    }

    /**
     * blocks classification - certified for high-rise residential properties
     * @param boolean $featuresClassificationBlocks
     * @return $this
     */
    public function setFeaturesClassificationBlocks(?bool $featuresClassificationBlocks) : self{
        $this->featuresClassificationBlocks = $featuresClassificationBlocks;
        return $this;
    }

    /**
     * public classification - classified for amenities (hospitals, schools, museums)
     * @param type $featuresClassificationPublic
     * @return $this
     */
    public function setFeaturesClassificationPublic(?string $featuresClassificationPublic) : self{
        $this->featuresClassificationPublic = $featuresClassificationPublic;
        return $this;
    }

    /**
     * 
     * @param integer $featuresFloorsBuildable
     * @return $this
     * @throws \Exception
     */
    public function setFeaturesFloorsBuildable(?int $featuresFloorsBuildable) : self{
        if (null != $featuresFloorsBuildable) {
            if (1 <= $featuresFloorsBuildable && 99 >= $featuresFloorsBuildable) {
                $this->featuresFloorsBuildable = (int)$featuresFloorsBuildable;
            } else {
                $this->setErrores("featuresFloorsBuildable debe estar entre 1 y 99 y es $featuresFloorsBuildable");
            }
        }
        return $this;
    }

    /**
     * nearest location in km
     * @param integer $featuresNearestLocationKm
     * @return $this
     */
    public function setFeaturesNearestLocationKm(?int $featuresNearestLocationKm) : self{
        if (null != $featuresNearestLocationKm) {
            if (0 <= $featuresNearestLocationKm && 99 >= $featuresNearestLocationKm) {
                $this->featuresNearestLocationKm = (int)$featuresNearestLocationKm;
            } else {
                $this->setErrores("featuresNearestLocationKm debe estar entre 1 y 99 y es $featuresNearestLocationKm");
            }
        }
        return $this;
    }

    /**
     * electricity availability
     * @param boolean $featuresUtilitiesElectricity
     * @return $this
     */
    public function setFeaturesUtilitiesElectricity(?bool $featuresUtilitiesElectricity) : self{
        $this->featuresUtilitiesElectricity = $featuresUtilitiesElectricity;
        return $this;
    }

    /**
     * natural gas availability
     * @param boolean $featuresUtilitiesNaturalGas
     * @return $this
     */
    public function setFeaturesUtilitiesNaturalGas(?bool $featuresUtilitiesNaturalGas) : self{
        $this->featuresUtilitiesNaturalGas = $featuresUtilitiesNaturalGas;
        return $this;
    }

    /**
     * road access availability
     * @param boolean $featuresUtilitiesRoadAccess
     * @return $this
     */
    public function setFeaturesUtilitiesRoadAccess(?bool $featuresUtilitiesRoadAccess) : self{
        $this->featuresUtilitiesRoadAccess = $featuresUtilitiesRoadAccess;
        return $this;
    }

    /**
     * sewerage availability
     * @param boolean $featuresUtilitiesSewerage
     * @return $this
     */
    public function setFeaturesUtilitiesSewerage(?bool $featuresUtilitiesSewerage) : self{
        $this->featuresUtilitiesSewerage = $featuresUtilitiesSewerage;
        return $this;
    }

    /**
     * sidewalk availability  - disponibilidad de la acera
     * @param boolean $featuresUtilitiesSidewalk
     * @return $this
     */
    public function setFeaturesUtilitiesSidewalk(?bool $featuresUtilitiesSidewalk) : self{
        $this->featuresUtilitiesSidewalk = $featuresUtilitiesSidewalk;
        return $this;
    }

    /**
     * street lighting availability
     * @param boolean $featuresUtilitiesStreetLighting
     * @return $this
     */
    public function setFeaturesUtilitiesStreetLighting(?bool $featuresUtilitiesStreetLighting) : self{
        $this->featuresUtilitiesStreetLighting = $featuresUtilitiesStreetLighting;
        return $this;
    }

    /**
     * water availability
     * @param boolean $featuresUtilitiesWater
     * @return $this
     */
    public function setFeaturesUtilitiesWater(?bool $featuresUtilitiesWater) : self{
        $this->featuresUtilitiesWater = $featuresUtilitiesWater;
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->featuresType && "" != $this->featuresAreaPlot;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene featuresType y/o featuresAreaPlot  {featuresType:$this->featuresType, featuresAreaPlot:$this->featuresAreaPlot}";
        }
        
        return ["verificacion" => $verificacion01, "errores" => $this->errores];
    }

}
