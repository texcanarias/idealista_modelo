<?php namespace inmotek\idealista\v6\model\feature;

class Feature implements \inmotek\idealista\v6\model\verificaciones {
    
    use \inmotek\idealista\v6\model\traitLimpieza;
    use \inmotek\idealista\v6\model\traitErrores;
    
    public ?string $featuresType = null;
    
    public function setFeaturesType(?string $featuresType) : self{
        $this->featuresType = $featuresType;
        return $this;
    }
    
    public function verificaciones() {
        return true;
    }
    
}
