<?php namespace inmotek\idealista\v6\model\feature;

class Storage extends \inmotek\idealista\v6\model\feature\Feature implements \inmotek\idealista\v6\model\verificaciones {

    use traits\featureAreaConstructed;
    
    use \inmotek\idealista\v6\model\traitLimpieza;

    public static string $TYPE_STORAGE = "storage";

    public ?bool $featuresAccess24h = null;
    public ?int $featuresAreaHeight = null;
    public ?bool $featuresLoadingDock = null;
    public ?bool $featuresSecurity24h = null;

    public function __construct() {
        $this->featuresType = self::$TYPE_STORAGE;
    }

    /**
     * 24 hr access - availability of 24 hr access
     * @param boolean $featuresAccess24h
     * @return $this
     */
    public function setFeaturesAccess24h(?bool $featuresAccess24h) : self{
        $this->featuresAccess24h = $featuresAccess24h;
        return $this;
    }

    /**
     * height area - área de altura
     * @param type $featuresAreaHeight
     * @return $this
     * @throws \Exception
     */
    public function setFeaturesAreaHeight(?int $featuresAreaHeight) : self{
        if (null != $featuresAreaHeight) {
            if (0 <= $featuresAreaHeight && 9 >= $featuresAreaHeight) {
                $this->featuresAreaHeight = (int)$featuresAreaHeight;
            } else {
                $this->setErrores("featuresAreaHeight debe estar entre 0 y 9 y es $featuresAreaHeight");
            }
        }
        return $this;
    }

    /**
     * loading dock - loading dock availability
     * @param boolean $featuresLoadingDock
     * @return $this
     */
    public function setFeaturesLoadingDock(?bool $featuresLoadingDock) : self{
        $this->featuresLoadingDock = $featuresLoadingDock;
        return $this;
    }

    /**
     * 24 hr security service
     * @param boolean $featuresSecurity24h
     * @return $this
     */
    public function setFeaturesSecurity24h(?bool $featuresSecurity24h) : self{
        $this->featuresSecurity24h = $featuresSecurity24h;
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->featuresType && "" != $this->featuresAreaConstructed;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene featuresType y/o featuresAreaConstructed  {featuresType:$this->featuresType, featuresAreaConstructed:$this->featuresAreaConstructed}";
        }
        
        return ["verificacion" => $verificacion01, "errores" => $this->errores];                
    }

}
