<?php namespace inmotek\idealista\v6\model\feature;

class Offices extends \inmotek\idealista\v6\model\feature\Feature implements \inmotek\idealista\v6\model\verificaciones {
    use traits\featureEnergyCertificate;
    use traits\featureConservation;
    use traits\featureOrientation;
    use traits\featureBuiltYear;
    use traits\featureAreaConstructed;
    use traits\featureAreaUsable;
    use traits\featureBathroomNumber;
    use traits\featureFloorsBuilding;
    use traits\featureSecurity;
    use traits\featureWindowsLocation;
    
    use \inmotek\idealista\v6\model\traitLimpieza;

    public static $TYPE_OFFICE = "office";

    public static $BATH_TOILETS = "toilets";
    public static $BATH_FULLEQUIPED = "fullEquiped";
    public static $BATH_BOTH = "both";

    public static $CONDITIONED_AIR_TYPE_NOT_AVAILABLE = "notAvailable";
    public static $CONDITIONED_AIR_TYPE_COLD = "cold";
    public static $CONDITIONED_AIR_TYPE_COLD_HEAT = "cold/heat";
    public static $CONDITIONED_AIR_TYPE_PREINSTALLATION = "preInstallation";

    public static $ROOMS_SPLITTED_UNKNOWN = "unknown";
    public static $ROOMS_SPLITTED_OPENPLAN = "openPlan";
    public static $ROOMS_SPLITTED_WITH_SCREENS = "withScreens";
    public static $ROOMS_SPLITTED_WITH_WALLS = "withWalls";
    

    //public $featuresType;
    public ?bool $featuresAccessControl = null;
    public ?bool $featuresBathroomInside = null;
    public ?string $featuresBathroomType = null; //["toilets","fullEquiped","both"]
    public ?bool $featuresBuildingAdapted = null;
    public ?bool $featuresConditionedAir = null;
    public ?string $featuresConditionedAirType = null; //["notAvailable","cold","cold/heat","preInstallation"]
    public ?bool $featuresDoorman = null;
    public ?bool $featuresEmergencyExit = null;
    public ?bool $featuresEmergencyLights = null;
    public ?bool $featuresEquippedKitchen = null;
    public ?bool $featuresExtinguishers = null;
    public ?bool $featuresFireDetectors = null;
    public ?bool $featuresFireDoors = null;
    public ?int $featuresFloorsProperty = null;
    public ?bool $featuresHeating = null;
    public ?bool $featuresHotWater = null;
    public ?int $featuresLiftNumber = null;
    public ?bool $featuresOfficeBuilding = null;
    public ?int $featuresParkingSpacesNumber = null;
    public ?string $featuresRoomsSplitted = null; //["unknown","openPlan","withScreens","withWalls"]
    public ?bool $featuresSecuritySystem = null;
    public ?bool $featuresSprinklers = null;
    public ?bool $featuresSuspendedCeiling = null;
    public ?bool $featuresSuspendedFloor = null;
    public ?bool $featuresStorage = null;
    public ?bool $featuresWindowsDouble = null;
    //public ?string $featuresWindowsLocation = null; //["interior","exterior"]

    public function __construct() {
        $this->featuresType = self::$TYPE_OFFICE;
    }

    /**
     * availability of access control system
     * @param boolean $featuresAccessControl
     * @return $this
     */
    public function setFeaturesAccessControl(?bool $featuresAccessControl) : self{
        $this->featuresAccessControl = $featuresAccessControl;
        return $this;
    }

    /**
     * bathroom inside the property is available
     * @param boolean $featuresBathroomInside
     * @return $this
     */
    public function setFeaturesBathroomInside(?bool $featuresBathroomInside) : self{
        $this->featuresBathroomInside = $featuresBathroomInside;
        return $this;
    }


    public function setFeaturesBathroomType(?string $featuresBathroomType) : self{
        $this->featuresBathroomType = $featuresBathroomType;
        return $this;
    }

    /**
     * adapted building
     * @param boolean $featuresBuildingAdapted
     * @return $this
     */
    public function setFeaturesBuildingAdapted(?bool $featuresBuildingAdapted) : self{
        $this->featuresBuildingAdapted = $featuresBuildingAdapted;
        return $this;
    }

    /**
     * has air conditioning
     * @param boolean $featuresConditionedAir
     * @return $this
     */
    public function setFeaturesConditionedAir(?bool $featuresConditionedAir) : self{
        $this->featuresConditionedAir = $featuresConditionedAir;
        return $this;
    }

    /**
     * air conditioning type
     * @param type $featuresConditionedAirType
     * @return $this
     */
    public function setFeaturesConditionedAirType(?string $featuresConditionedAirType) : self{
        $this->featuresConditionedAirType = $featuresConditionedAirType;
        return $this;
    }

    /**
     * doorman availability
     * @param boolean $featuresDoorman
     * @return $this
     */
    public function setFeaturesDoorman(?bool $featuresDoorman) : self {
        if(null != $featuresDoorman){
            $this->featuresDoorman = $featuresDoorman;
        }
        return $this;
    }

    /**
     * emergency exit
     * @param boolean $featuresEmergencyExit
     * @return $this
     */
    public function setFeaturesEmergencyExit(?bool $featuresEmergencyExit) : self{
        $this->featuresEmergencyExit = $featuresEmergencyExit;
        return $this;
    }

    /**
     * emergency lights
     * @param boolean $featuresEmergencyLights
     * @return $this
     */
    public function setFeaturesEmergencyLights(?bool $featuresEmergencyLights) :self{
        $this->featuresEmergencyLights = $featuresEmergencyLights;
        return $this;
    }

    /**
     * equipped with kitchen only
     * @param boolean $featuresEquippedKitchen
     * @return $this
     */
    public function setFeaturesEquippedKitchen(?bool $featuresEquippedKitchen) : self{
        $this->featuresEquippedKitchen = $featuresEquippedKitchen;
        return $this;
    }

    /**
     * extinguishers availability
     * @param boolean $featuresExtinguishers
     * @return $this
     */
    public function setFeaturesExtinguishers(?bool $featuresExtinguishers) : self{
        $this->featuresExtinguishers = $featuresExtinguishers;
        return $this;
    }

    /**
     * availability of fire detectors
     * @param boolan $featuresFireDetectors
     * @return $this
     */
    public function setFeaturesFireDetectors(?bool $featuresFireDetectors) : self{
        $this->featuresFireDetectors = $featuresFireDetectors;
        return $this;
    }

    /**
     * availability of fire doors
     * @param type $featuresFireDoors
     * @return $this
     */
    public function setFeaturesFireDoors(?bool $featuresFireDoors) : self{
        $this->featuresFireDoors = $featuresFireDoors;
        return $this;
    }

    /**
     * property floors number
     * @param integer $featuresFloorsProperty
     * @return $this
     */
    public function setFeaturesFloorsProperty(?int $featuresFloorsProperty) : self{
        if (null != $featuresFloorsProperty) {
            if (1 <= $featuresFloorsProperty && 99 >= $featuresFloorsProperty) {
                $this->featuresFloorsProperty = (int)$featuresFloorsProperty;
            } else {
                $this->setErrores("featuresFloorsProperty debe estar entre 1 y 99 y es $featuresFloorsProperty");
            }
        }
        return $this;
    }

    /**
     * heating availability
     * @param boolean $featuresHeating
     * @return $this
     */
    public function setFeaturesHeating(?bool $featuresHeating) : self{
        $this->featuresHeating = $featuresHeating;
        return $this;
    }

    /**
     * hot water availability
     * @param boolean $featuresHotWater
     * @return $this
     */
    public function setFeaturesHotWater(?bool $featuresHotWater) : self{
        $this->featuresHotWater = $featuresHotWater;
        return $this;
    }

    /**
     * lifts number
     * @param type $featuresLiftNumber
     * @return $this
     */
    public function setFeaturesLiftNumber(?int $featuresLiftNumber) : self{
        if (null != $featuresLiftNumber) {
            if (1 <= $featuresLiftNumber && 99 >= $featuresLiftNumber) {
                $this->featuresLiftNumber = (int)$featuresLiftNumber;
            } else {
                $this->setErrores("featuresLiftNumber debe estar entre 1 y 99 y es $featuresLiftNumber");
            }
        }
        return $this;
    }

    /**
     * office building
     * @param boolean $featuresOfficeBuilding
     * @return $this
     */
    public function setFeaturesOfficeBuilding(?bool $featuresOfficeBuilding) : self{
        $this->featuresOfficeBuilding = $featuresOfficeBuilding;
        return $this;
    }

    /**
     * parking spaces number
     * @param integer $featuresParkingSpacesNumber
     * @return $this
     */
    public function setFeaturesParkingSpacesNumber(?int $featuresParkingSpacesNumber) : self{
        if (null != $featuresParkingSpacesNumber) {
            if (1 <= $featuresParkingSpacesNumber && 99 >= $featuresParkingSpacesNumber) {
                $this->featuresParkingSpacesNumber = (int)$featuresParkingSpacesNumber;
            } else {
                $this->setErrores("featuresParkingSpacesNumber debe estar entre 1 y 99 y es $featuresParkingSpacesNumber");
            }
        }
        return $this;
    }

    /**
     * rooms splitted
     * @param string $featuresRoomsSplitted
     * @return $this
     */
    public function setFeaturesRoomsSplitted(?string $featuresRoomsSplitted) : self{
        $this->featuresRoomsSplitted = $featuresRoomsSplitted;
        return $this;
    }


    /**
     * security system
     * @param boolean $featuresSecuritySystem
     * @return $this
     */
    public function setFeaturesSecuritySystem(?bool $featuresSecuritySystem) : self{
        $this->featuresSecuritySystem = $featuresSecuritySystem;
        return $this;
    }

    /**
     * sprinklers (aspersores)
     * @param boolean $featuresSprinklers
     * @return $this
     */
    public function setFeaturesSprinklers(?bool $featuresSprinklers) : self {
        $this->featuresSprinklers = $featuresSprinklers;
        return $this;
    }

    /**
     * suspending floor
     * @param boolean $featuresSuspendedCeiling
     * @return $this
     */
    public function setFeaturesSuspendedCeiling(?bool $featuresSuspendedCeiling) : self {
        $this->featuresSuspendedCeiling = $featuresSuspendedCeiling;
        return $this;
    }

    /**
     * suspending floor
     * @param boolean $featuresSuspendedFloor
     * @return $this
     */
    public function setFeaturesSuspendedFloor(?bool $featuresSuspendedFloor) : self{
        $this->featuresSuspendedFloor = $featuresSuspendedFloor;
        return $this;
    }

    /**
     * storage room
     * @param boolean $featuresStorage
     * @return $this
     */
    public function setFeaturesStorage(?bool $featuresStorage) : self{
        $this->featuresStorage = $featuresStorage;
        return $this;
    }

    /**
     * double pane windows availability
     * @param boolean $featuresWindowsDouble
     * @return $this
     */
    public function setFeaturesWindowsDouble(?bool $featuresWindowsDouble) : self{
        $this->featuresWindowsDouble = $featuresWindowsDouble;
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->featuresType && "" != $this->featuresAreaConstructed;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene featuresType y/o featuresAreaConstructed {featuresType:$this->featuresType, featuresAreaConstructed:$this->featuresAreaConstructed}";
        }
        
        return ["verificacion" => $verificacion01, "errores" => $this->errores];        
    }

}
