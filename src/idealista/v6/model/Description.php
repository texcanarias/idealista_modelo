<?php namespace inmotek\idealista\v6\model;

class Description  implements verificaciones{
    
    use \inmotek\idealista\v6\model\traitLimpieza;
    use \inmotek\idealista\v6\model\traitErrores;

    static public string $ES = "spanish";
    static public string $IT = "italian";
    static public string $PT = "portuguese";
    static public string $EN = "english";
    static public string $GE = "german";
    static public string $FR = "french";
    static public string $RU = "russian";
    static public string $CH = "chinese";
    static public string $CA = "catalan";
    static public string $FI = "finnish";
    static public string $DU = "dutch";
    static public string $PO = "polish";
    static public string $RO = "romanian";
    static public string $SW = "swedish";

    public string $descriptionLanguage = "";
    public string $descriptionText = "";

    private function __construct(string $descriptionLanguage, string $descriptionText){
        $this->setDescriptionLanguage($descriptionLanguage);
        $this->setDescriptionText($descriptionText);
    }

    public function set(string $descriptionLanguage, string $descriptionText) : self{
        $d = new self($descriptionLanguage,$descriptionText);
        return $d;
    }


    public function setDescriptionLanguage(string $descriptionLanguage) : self{
        $this->descriptionLanguage = $descriptionLanguage;
        return $this;
    }

    public function setDescriptionText(string $descriptionText) : self{
        $this->descriptionText = mb_substr(strip_tags(trim($descriptionText)), 0, 4000);
        return $this;
    }


    public function verificaciones() {
        $verificacion01 = "" != $this->descriptionLanguage;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene descriptionLanguage {descriptionLanguage:$this->descriptionLanguage}";
        }
        
        return ["verificacion" => $verificacion01 , "errores" => $this->errores];         
    }
}
