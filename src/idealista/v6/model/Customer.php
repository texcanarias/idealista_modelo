<?php namespace inmotek\idealista\v6\model;

class Customer  implements verificaciones{
    public string $customerSendDate = "";
    public string $customerCountry = "";
    public string $customerCode = "";
    public string $customerName = "";
    public string $customerReference = "";
    
    use \inmotek\idealista\v6\model\traitLimpieza;
    use \inmotek\idealista\v6\model\traitErrores;

    /*
     * \inmotek\idealista\v6\model\Contact
     */
    public \inmotek\idealista\v6\model\Contact $customerContact;

    /**
     * Coleccion de objetos de tipo \inmotek\idealista\v6\model\Property
     * array
     */
    public $customerProperties;

    /**
     * Coleccion de objetos de tipo \inmotek\idealista\v6\model\NewDevelopment
     * array
     */
    public $customerNewDevelopments;

    public function __construct() {
        $this->customerProperties = [];
        $this->customerNewDevelopments = [];
        $this->customerCountry = "Spain";
        $this->customerSendDate = date("Y/m/d h:m:s");
    }

    public function setCustomerCode(string $customerCode) : self{
      $this->customerCode = "";
        $preg = "/ilc([a-z]|[0-9]){40}/";
        if (preg_match($preg, $customerCode)) {
            $this->customerCode = $customerCode;
        } else {
            $this->setErrores("customerCode no cumple con su pattern -> " . $customerCode . " " . $preg);
        }
        return $this;
    }

    public function setCustomerName(string $customerName) : self{
        $this->customerName = mb_substr($customerName,0,100);
        return $this;
    }

    /**
     * Customer Reference
     * @param string $customerReference
     * @return $this
     */
    public function setCustomerReference(string $customerReference) : self{
        $this->customerReference = mb_substr($customerReference,0,50);
        return $this;
    }

    /**
     * Datos de contacto
     * @param \inmotek\idealista\v6\model\Contact $customerContact
     * @return $this
     */
    public function setCustomerContact(\inmotek\idealista\v6\model\Contact $customerContact): self {
        $this->customerContact = $customerContact;
        return $this;
    }

    /**
     * @param array customerProperties
     */
    public function setCustomerProperties($customerProperties) : self{
        $this->customerProperties = $customerProperties;
        return $this;
    }

    /**
     * @param array customerNewDevelopments
     */
    public function setCustomerNewDevelopments($customerNewDevelopments) : self{
        $this->customerNewDevelopments = $customerNewDevelopments;
        return $this;
    }

    public function verificaciones() {
        $verificacion01 = "" != $this->customerCountry && "" != $this->customerCode;
        $verificacion02 = "" != $this->customerCountry && "" != $this->customerReference;
        
        if(!$verificacion01){
            $this->errores[] = "No tiene customerCountry y/o customerCode  {customerCountry:$this->customerCountry, customerCode:$this->customerCode}";
        }
        if(!$verificacion02){
            $this->errores[] = "No tiene customerCountry y/o customerReference {customerCountry:$this->customerCountry, customerReference:$this->customerReference}";
        }       
        
        return ["verificacion" => $verificacion01 || $verificacion02, "errores" => $this->errores];         
    }

}
