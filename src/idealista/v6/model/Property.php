<?php namespace inmotek\idealista\v6\model;

class Property extends CorePropertyTypology implements verificaciones {

    public \inmotek\idealista\v6\model\Operation $propertyOperation;
    
    public function __construct() {
        parent::__construct();
    }

    public function setPropertyOperation(\inmotek\idealista\v6\model\Operation $propertyOperation) : self{
        $this->propertyOperation = $propertyOperation;
        return $this;
    }


    public function verificaciones() {
        return parent::verificaciones();
    }

}
