<?php

declare(strict_types=1);

namespace inmotek_test\phpunit;

use PHPUnit\Framework\TestCase;
use\inmotek\idealista\v6\model\{Customer, Contact, Property, CoreProperty, Operation, Address, Description, Image, NewDevelopment};
use \inmotek\idealista\v6\model\feature\Feature;
use\inmotek\idealista\v6\model\feature\{Building, Garage, Homes, Land, Offices, Premises, storage};

final class EstructuraTest extends TestCase
{
    public function testConn()
    {
        $Customer = new Customer();
        $Customer->setCustomerName('portalmas');
        $Customer->setCustomerReference("222");
        $Customer->setCustomerCode('ilcfa2cea03757ec8bbff317d9d3c94326df57c3071');
        $Customer->setCustomerContact($this->getContact());
        $Customer->setCustomerProperties($this->getPropertyCollection());
        $Customer->setCustomerNewDevelopments($this->getPromotionCollection());            
        $noTienePropiedades = 0 == count($Customer->customerProperties);
        if ($noTienePropiedades) {
            $Customer->customerProperties = null;
        }

        if (!$Customer->verificaciones()['verificacion']) {
            $itemLog = '[customer] ' . $Customer->customerName . "|" . implode(",", $Customer->verificaciones()['errores']) . "\n";
            echo $itemLog;
        }





        /*$result =  json_decode(json_encode($Customer), true);
        print_r($result);*/

        $this->assertTrue('portalmas' == $Customer->customerName);
    }

    private function getContact()
    {
        try {
            $Contacts = new Contact();
            $Contacts->setContactName('Antonio Teixeira');
            $Contacts->setContactEmail('antonio@saresoft.com');
            $Contacts->setContactPrimaryPhonePrefix('34');
            $Contacts->setContactPrimaryPhoneNumber('999888776655');
            $Contacts->setContactSecondaryPhonePrefix('34');
            $Contacts->setContactSecondaryPhoneNumber('999888776655');
            if (!$Contacts->verificaciones()['verificacion']) {
                print_r($Contacts->getErrores());
                throw new \Exception("No se ha generado correctamente el contacto");
            }
            $Contacts->limpieza();
            return $Contacts;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function getPropertyCollection()
    {
        $propertyCollection = [];

        $property = new Property();
        $property->propertyCode = 'portalmas_20';
        $property->propertyReference = 'portalmas_20_v';
        $property->setPropertyVisibility(CoreProperty::$VISIBILITY_IDEALISTA);
        $property->setPropertyUrl('http://www.portalmas.es/20');
        $property->setPropertyOperation($this->operation());
        $property->setPropertyAddress($this->address());
        $property->setPropertyContact($this->propertyContact());
        $property->setPropertyDescriptions($this->description());
        $property->setPropertyImages($this->images());
        $property->setPropertyFeatures($this->feature());

        $propertyCollection[] = $property;

        return $propertyCollection;
    }

    private function operation()
    {
        $objOperation = new \inmotek\idealista\v6\model\Operation();
        $objOperation->setOperationPrice(100);
        $objOperation->setOperationType(Operation::$RENT);

        /*$isAlquilerFianzaEnMeses = "" != $value['operation']['fianza_cantidad'] && "m" == $value['operation']['fianza_tipo'];
        if($isAlquilerFianzaEnMeses){
            $objOperation->setOperationDepositMonths($value['operation']['fianza_cantidad']);
        }*/

        if (!$objOperation->verificaciones()['verificacion'] || $objOperation->isError()) {
        }

        $objOperation->limpieza();
        return $objOperation;
    }

    private function address()
    {
        $address = new Address();
        $address->setAddressVisibility(Address::$VISIBILITY_FULL);
        $address->setAddressCountry(Address::$COUNTRY_ES);
        $address->setAddressStreetName("Don Pío Coronado 172");
        $address->setAddressStreetNumber('172');
        $address->setAddressBlock("");
        $address->setAddressStair("2");
        $address->setAddressDoor("A");
        $address->setAddressPostalCode("35012");
        $address->setAddressTown("Las Palmas de Gran Canaria");
        $address->setAddressFloor("");
        $address->setAddressCoordinatesPrecision(Address::$PRECISION_EXACT);
        $address->setAddressCoordinatesLatitude(20.505050);
        $address->setAddressCoordinatesLongitude(-0.101010);
        if (!$address->verificaciones()['verificacion']) {
        }

        $address->limpieza();

        return $address;
    }

    private function propertyContact()
    {
        $contact = new \inmotek\idealista\v6\model\Contact();
        $contact->setContactName("Juan de Dios");
        $contact->setContactEmail("juandedios@saresoft.com");
        $contact->setContactPrimaryPhoneNumber("999888776655");
        $contact->setContactSecondaryPhoneNumber("");
        if (!$contact->verificaciones()['verificacion']) {
        }
        $contact->limpieza();

        return $contact;
    }

    private function description()
    {
        $descriptionCollection = [];
        $objDescription = Description::set(Description::$ES, "Texto ejemplo en ES");
        if (!$objDescription->verificaciones()['verificacion']) {
        }
        $descriptionCollection[] = $objDescription;

        $objDescription = Description::set(Description::$EN, "EN sample text");
        if (!$objDescription->verificaciones()['verificacion']) {
        }
        $descriptionCollection[] = $objDescription;

        return $descriptionCollection;
    }

    private function images()
    {
        $conversionTitle = [
            "sotano" => Image::$BASEMENT, "aseo" => Image::$BATHROOM, "bano" => Image::$BATHROOM,
            "dormitorio" => Image::$BEDROOM, "dormitorio1" => Image::$BEDROOM, "habitacionservicio" => Image::$BEDROOM,
            "bodega" => Image::$CELLAR, "zonascomunes" => Image::$COMMUNAL_AREAS, "comedor" => Image::$DINING_ROOM,
            "exterior" => Image::$FACADE, "iexterior" => Image::$FACADE, "garaje" => Image::$GARAGE,
            "pasillo" => Image::$HALL, "hall" => Image::$HALL, "cocina" => Image::$KITCHEN, "salacine" => Image::$LIVING,
            "sala" => Image::$LIVING, "salon" => Image::$LIVING, "despacho" => Image::$OFFICE, "patio" => Image::$PATIO,
            "atico" => Image::$PENTHOUSE, "plano" => Image::$PLAN, "piscina" => Image::$POOL, "porche" => Image::$PORCH,
            "recepcion" => Image::$RECEPTION, "trastero" => Image::$STORAGE, "almacen" => Image::$STORAGE,
            "calle" => Image::$SURROUNDINGS, "exterior" => Image::$FACADE, "terraza" => Image::$TERRACE,
            "despensa" => Image::$UNKNOWN, "distribuidor" => Image::$UNKNOWN, "galeria" => Image::$UNKNOWN,
            "gym" => Image::$UNKNOWN, "jacuzzi" => Image::$UNKNOWN, "mirador" => Image::$UNKNOWN,
            "sauna" => Image::$UNKNOWN, "spa" => Image::$UNKNOWN, "vestidor" => Image::$UNKNOWN,
            "vestibulo" => Image::$UNKNOWN, "cenital" => Image::$UNKNOWN, "interior" => Image::$UNKNOWN,
            "escaparate" => Image::$UNKNOWN, "jardin" => Image::$UNKNOWN, "detalle" => Image::$DETAILS,
            "escaparate" => Image::$UNKNOWN, "parcela" => Image::$UNKNOWN, "vistas" => Image::$VIEWS
        ];


        $imageCollection = array();

        $objImagen = new Image();
        $objImagen->setImageLabel(Image::$UNKNOWN);
        $objImagen->setImageOrder(1);
        $objImagen->setImageUrl("http://www.google.es/img.jpeg");
        if (!$objImagen->verificaciones()['verificacion']) {
        }

        $objImagen->limpieza();
        $imageCollection[] = $objImagen;
        return $imageCollection;
    }

    private function feature()
    {
        $type = 'storage';
        switch ($type) {
            case 'building':
                $obj = $this->getFeatureBuilding();
                break;
            case 'garage':
                $obj = $this->getFeatureGarage();
                break;
            case 'flat':
            case 'house':
            case 'countryHouse':
                $obj = $this->getFeatureHomes();
                break;
            case 'land':
                $obj = $this->getFeatureLand();
                break;
            case 'office':
                $obj = $this->getFeatureOffices();
                break;
            case 'premise':
                $obj = $this->getFeaturePremise();
                break;
            case 'storage':
                $obj = $this->getFeatureStorage();
                break;
        }
        if (!$obj->verificaciones()['verificacion'] || $obj->isError()) {
        }
        $obj->limpieza();

        return $obj;
    }

    /**
     * setea las propiedades relacionada con el certificado energético
     * @param \inmotek\idealista\model\feature\Feature $obj
     * @param array $value
     */
    private function EnergyCertificate(Feature &$obj)
    {
        try {
            $obj->setFeaturesEnergyCertificateRating(Building::$RATING_A);
            $obj->setFeaturesEnergyCertificatePerformance(Building::$RATING_B);
            $obj->setFeaturesEnergyCertificateType(Building::$TYPE_COMPLETED);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function Orientation(Feature &$obj)
    {
        try {
            $obj->setFeaturesOrientationNorth(true);
            $obj->setFeaturesOrientationSouth(false);
            $obj->setFeaturesOrientationWest(false);
            $obj->setFeaturesOrientationEast(false);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function getFeatureBuilding()
    {
        try {
            //Este tipo parece que no está en inmotek o no se recoge con esta aplicación. Se queda reservado.
            $obj = new Building();

            $this->EnergyCertificate($obj);
            $obj->setFeaturesConservation(BUILDING::$CONSERVATION_NEW);
            $obj->setFeaturesBuiltYear(2015);
            $obj->setFeaturesAreaConstructed(100);
            $obj->setFeaturesLiftNumber(2);
            /* $obj->setFeaturesAreaTradableMinimum();


              $obj->setFeaturesClassificationChalet();
              $obj->setFeaturesClassificationCommercial();
              $obj->setFeaturesClassificationHotel();
              $obj->setFeaturesClassificationIndustrial();
              $obj->setFeaturesClassificationOffice();
              $obj->setFeaturesClassificationOther();

              $obj->setFeaturesBuiltProperties();
              $obj->setFeaturesFloorsBuilding();
              $obj->setFeaturesParkingSpacesNumber();
              $obj->setFfeaturesPropertyTenants();
              $obj->setFfeaturesSecurityPersonnel(); */
            return $obj;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function getFeatureGarage()
    {
        try {
            $obj = new Garage();
            $obj->setFeaturesAreaConstructed(100);
            $obj->setFeaturesLiftAvailable(true);
            /*
              $obj->setFeaturesParkingAutomaticDoor("");
              $obj->setFeaturesParkingPlaceCovered("");
              $obj->setFeaturesSecurityAlarm("");
              $obj->setFeaturesSecurityPersonnel("");
              $obj->setFeaturesSecuritySystem(""); */
            return $obj;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function getFeatureHomes()
    {
        try {
            $obj = new \inmotek\idealista\v6\model\feature\Homes();
            $this->EnergyCertificate($obj);
            $this->Orientation($obj);

            $obj->setFeaturesType(Homes::$TYPE_FLAT);

            $obj->setFeaturesConservation(Homes::$CONSERVATION_GOOD);


            $obj->setFeaturesBuiltYear(1993);
            $obj->setFeaturesAreaConstructed(100);

            $obj->setFeaturesAreaPlot(50);
            $obj->setFeaturesAreaUsable(100);
            $obj->setFeaturesBalcony(true);
            $obj->setFeaturesBathroomNumber(2);
            $obj->setFeaturesBedroomNumber(2);
            $obj->setFeaturesConditionedAir(true);
            $obj->setFeaturesChimney(true);
            $obj->setFeaturesDoorman(false);
            $obj->setFeaturesDuplex(false);
            $obj->setFeaturesEmergencyExit(false);
            /*  $obj->setFeaturesEmergencyLights("");
              $obj->setFeaturesEquippedKitchen(""); */
            $obj->setFeaturesEquippedWithFurniture(true);
            $obj->setFeaturesFloorsBuilding(5);
            $obj->setFeaturesFloorsInTop(true);
            $obj->setFeaturesGarden(true);
            //$obj->setFeaturesHandicapAdaptedAccess("");
            $obj->setFeaturesHandicapAdaptedUse(true);
            $obj->setFeaturesLiftAvailable(true);
            $obj->setFeaturesParkingAvailable(true);
            $obj->setFeaturesPenthouse(false);
            $obj->setFeaturesPetsAllowed(false);
            $obj->setFeaturesPool(false);
            $obj->setFeaturesRooms(4);
            $obj->setFeaturesStorage(true);
            $obj->setFeaturesStudio(true);
            $obj->setFeaturesTerrace(true);
            $obj->setFeaturesWardrobes(true);
            $obj->setFeaturesWindowsLocation(Homes::$INTERIOR);
            $obj->setFeaturesHeatingType(Homes::$HEATING_TYPE_CENTRAL_GAS);

            return $obj;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function getFeatureLand()
    {
        try {
            $obj = new \inmotek\idealista\v6\model\feature\Land();
            $obj->setFeaturesAreaPlot(200);
            $obj->setFeaturesAreaBuildable(100);
            $obj->setFeaturesType(Land::$TYPE_LAND_URBAN);

            /*

              $obj->setFeaturesClassificationChalet(false);
              $obj->setFeaturesClassificationCommercial(false);
              $obj->setFeaturesClassificationHotel(false);*/
            //$isIndustrial = (4 == $value['features']['calification']);
            $obj->setFeaturesClassificationIndustrial(false);
            /*
              $obj->setFeaturesClassificationOffice(false);
              $obj->setFeaturesClassificationOther(false);

              $obj->setFeaturesClassificationBlocks(false);
              $obj->setFeaturesClassificationPublic(false);

              $obj->setFeaturesFloorsBuildable("");
              $obj->setFeaturesNearestLocationKm(""); */
            $obj->setFeaturesUtilitiesElectricity(true);
            /* $obj->setFeaturesUtilitiesNaturalGas(false);
              $obj->setFeaturesUtilitiesRoadAccess(false);*/
            $obj->setFeaturesUtilitiesSewerage(true);
            /*$obj->setFeaturesUtilitiesSidewalk(false);
              $obj->setFeaturesUtilitiesStreetLighting(false);
              $obj->setFeaturesUtilitiesWater(false);
              $obj->setFeaturesAccessType(""); */

            return $obj;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function getFeatureOffices()
    {
        try {
            $obj = new \inmotek\idealista\v6\model\feature\Offices();

            $this->EnergyCertificate($obj);
            $this->Orientation($obj);
            $obj->setFeaturesConservation(Offices::$CONSERVATION_NEW);
            $obj->setFeaturesBuiltYear(2015);
            $obj->setFeaturesAreaConstructed(100);
            $obj->setFeaturesAreaUsable(90);
            $obj->setFeaturesBathroomNumber(5);
            $obj->setFeaturesConditionedAir(true);
            $obj->setFeaturesConditionedAirType(Offices::$CONDITIONED_AIR_TYPE_COLD_HEAT);
            $obj->setFeaturesDoorman(true);
            $obj->setFeaturesExtinguishers(false);
            $obj->setFeaturesFireDetectors(false);
            $obj->setFeaturesFloorsBuilding(5);
            $obj->setFeaturesHeating(true);
            $obj->setFeaturesHotWater(true);
            $obj->setFeaturesBathroomInside(true);
            $obj->setFeaturesWindowsLocation(Offices::$EXTERIOR);
            $obj->setFeaturesLiftNumber(5);

            /*
              $obj->setFeaturesType("");
              $obj->setFeaturesAccessControl(""); */
            /* $obj->setFeaturesBathroomType(""); //["toilets","fullEquiped","both"]
              $obj->setFeaturesBuildingAdapted(""); */
            /* $obj->setFeaturesEmergencyExit();
              $obj->setFeaturesEmergencyLights();
              $obj->setFeaturesEquippedKitchen();
              $obj->setFeaturesFireDoors("");
              $obj->setFeaturesFloorsProperty();
              $obj->setFeaturesOfficeBuilding("");
              $obj->setFeaturesParkingSpacesNumber("");
              $obj->setFeaturesRoomsSplitted(""); //["unknown","openPlan","withScreens","withWalls"]
              $obj->setFeaturesSecurityAlarm("");
              $obj->setFeaturesSecurityDoor("");
              $obj->setFeaturesSecuritySystem("");
              $obj->setFeaturesSprinklers("");
              $obj->setFeaturesSuspendedCeiling("");
              $obj->setFeaturesSuspendedFloor("");
              $obj->setFeaturesStorage("");
              $obj->setFeaturesWindowsDouble(""); */
            return $obj;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function getFeaturePremise()
    {
        try {
            $obj = new \inmotek\idealista\v6\model\feature\Premises();
            $this->EnergyCertificate($obj);
            $obj->setFeaturesType(Premises::$TYPE_PREMISES_INDUSTRIAL);
            $obj->setFeaturesConservation(Premises::$CONSERVATION_NEW);
            $obj->setFeaturesAreaConstructed(100);
            $obj->setFeaturesAreaUsable(99);
            $obj->setFeaturesBathroomNumber(2);
            $obj->setFeaturesConditionedAir(true);
            $obj->setFeaturesHeating(true);
            $obj->setFeaturesSmokeExtraction(true);
            $obj->setFeaturesRooms(5);
            $obj->setFeaturesFloorsBuilding(5);
            $obj->setFeaturesSecurityAlarm(true);
            $obj->setFeaturesSecurityDoor(true);
            $obj->setFeaturesStorage(true);

            /*
              $obj->setFeaturesAreaPlot("");
              $obj->setFeaturesBathroomAdapted("");
              $obj->setFeaturesEquippedKitchen("");
              $obj->setFeaturesFacadeArea("");
              $obj->setFeaturesLastActivity("");
              $obj->setFeaturesLocatedAtCorner("");
              $obj->setFeaturesSecuritySystem("");
              $obj->setFeaturesUbication(""); //["shopping","street","mezzanine","belowGround","other","unknown"]
              $obj->setFeaturesWindowsNumber(""); */

            return $obj;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function getFeatureStorage()
    {
        try {
            $obj = new \inmotek\idealista\v6\model\feature\Storage();
            $obj->setFeaturesAreaConstructed(100);
            /* $obj->setFeaturesAccess24h();
              $obj->setFeaturesAreaHeight();
              $obj->setFeaturesLoadingDock();
              $obj->setFeaturesSecurity24h(); */
            return $obj;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

        /**
     * Listado de las promociones de la agencia a sincronizar
     * @param type $data
     * @param array
     */
    private function getPromotionCollection() {
        $promotion = new \inmotek\idealista\v6\model\NewDevelopment();
        $promotion->setPropertyCode('A-200');
        $promotion->setPropertyReference('A-200');
        $promotion->setPropertyVisibility(NewDevelopment::$VISIBILITY_MICROSITE);
        $promotion->setPropertyUrl('http://www.promocion.com/');
        $promotion->setPropertyFeatures($this->getPromoFeature()); //@todo Resolver este problema en la estrucutra de clases!!
        $promotion->setPropertyAddress($this->address());
        $promotion->setPropertyContact($this->getContact());
        $promotion->setPropertyDescriptions($this->description());
        $promotion->setPropertyImages($this->images());
        $promotion->setNewDevelopmentTypologies($this->getPropertyCollectionPromo());
        return $promotion;
    }

    private function getPromoFeature() : \inmotek\idealista\v6\model\Promo{
        $promoFeature = new \inmotek\idealista\v6\model\Promo();
        $promoFeature->setFeaturesNewDevelopmentName('Promocion Loca');
        $promoFeature->setFeaturesDoorman(false);
        $promoFeature->setFeaturesPool(false);
        $promoFeature->setFeaturesStartDate("2020-01-01");
        $promoFeature->setFeaturesKeyDeliveryMonth(5);
        $promoFeature->setFeaturesKeyDeliveryYear(1);
        $promoFeature->setFeaturesAccessComments([]);
        $promoFeature->limpieza();

        return $promoFeature; 
    }

    private function getPropertyCollectionPromo()
    {
        $propertyCollection = [];

        $property = new Property();
        $property->propertyCode = 'portalmas_20';
        $property->propertyReference = 'portalmas_20_v';
        $property->setPropertyVisibility(CoreProperty::$VISIBILITY_IDEALISTA);
        $property->setPropertyUrl('http://www.portalmas.es/20');
        $property->setPropertyOperation($this->operation());
        $property->setPropertyAddress($this->address());
        $property->setPropertyContact($this->propertyContact());
        $property->setPropertyDescriptions($this->description());
        $property->setPropertyImages($this->images());
        $property->setPropertyFeatures($this->featurePromo());

        $propertyCollection[] = $property;

        return $propertyCollection;
    }

    /**
     * Tipo de inmuebles, solo están los admitidos
     */
    private function featurePromo()
    {
        $type = 'garage';
        switch ($type) {
            case 'garage':
                $obj = $this->getFeatureGarage();
                break;
            case 'flat':
            case 'house':
            case 'countryHouse':
                $obj = $this->getFeatureHomes();
                break;
            case 'office':
                $obj = $this->getFeatureOffices();
                break;
            case 'premise':
                $obj = $this->getFeaturePremise();
                break;
        }
        if (!$obj->verificaciones()['verificacion'] || $obj->isError()) {
        }
        $obj->limpieza();

        return $obj;
    }

}
